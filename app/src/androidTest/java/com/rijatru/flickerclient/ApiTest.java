package com.rijatru.flickerclient;

import com.rijatru.flickerclient.dependencyInjection.component.DaggerTestAppComponent;
import com.rijatru.flickerclient.dependencyInjection.component.TestAppComponent;
import com.rijatru.flickerclient.dependencyInjection.module.AppModule;
import com.rijatru.flickerclient.dependencyInjection.module.TestDataModule;
import com.rijatru.flickerclient.model.data.interfaces.ApiManager;
import com.rijatru.flickerclient.model.data.interfaces.ServiceManager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static android.support.test.InstrumentationRegistry.getInstrumentation;

public class ApiTest {

    private App app;

    @Inject
    ServiceManager serviceManager;
    @Inject
    ApiManager apiManager;

    public ApiTest() {
        super();
    }

    @Before
    public void setUp() throws Exception {
        System.setProperty("dexmaker.dexcache", getInstrumentation().getTargetContext().getCacheDir().getPath());

        TestAppComponent testComponent = DaggerTestAppComponent.builder()
                .appModule(new AppModule((App) getInstrumentation().getTargetContext().getApplicationContext()))
                .testDataModule(new TestDataModule())
                .build();

        testComponent.inject(this);

        app = ((FlickerApp) getInstrumentation().getTargetContext().getApplicationContext());
    }

    @Test
    public void testGetRecentPhotosIsNotNull() {
        serviceManager.getRecentPhotos(photos -> Assert.assertTrue(photos != null));
    }
}
