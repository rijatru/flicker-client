package com.rijatru.flickerclient;

import android.text.TextUtils;

import com.rijatru.flickerclient.dependencyInjection.component.DaggerTestAppComponent;
import com.rijatru.flickerclient.dependencyInjection.component.TestAppComponent;
import com.rijatru.flickerclient.dependencyInjection.module.AppModule;
import com.rijatru.flickerclient.dependencyInjection.module.TestDataModule;
import com.rijatru.flickerclient.model.Photo;
import com.rijatru.flickerclient.model.data.interfaces.SerializationManager;
import com.rijatru.flickerclient.model.data.utils.TextUtil;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static android.support.test.InstrumentationRegistry.getInstrumentation;

public class SerializationTest {

    private App app;

    @Inject
    SerializationManager serializationManager;

    public SerializationTest() {
        super();
    }

    @Before
    public void setUp() throws Exception {
        System.setProperty("dexmaker.dexcache", getInstrumentation().getTargetContext().getCacheDir().getPath());

        TestAppComponent testComponent = DaggerTestAppComponent.builder()
                .appModule(new AppModule((App) getInstrumentation().getTargetContext().getApplicationContext()))
                .testDataModule(new TestDataModule())
                .build();

        testComponent.inject(this);

        app = ((FlickerApp) getInstrumentation().getTargetContext().getApplicationContext());
    }

    @Test
    public void testSerialization() {
        Photo photo1 = new Photo();
        String json = serializationManager.getJson(photo1);
        Photo photo2 = (Photo) serializationManager.fromJson(json, Photo.class);
        String json2 = serializationManager.getJson(photo2);
        Assert.assertEquals(json, json2);
    }
}
