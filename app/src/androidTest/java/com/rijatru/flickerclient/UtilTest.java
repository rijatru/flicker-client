package com.rijatru.flickerclient;

import android.text.TextUtils;

import com.rijatru.flickerclient.dependencyInjection.component.DaggerTestAppComponent;
import com.rijatru.flickerclient.dependencyInjection.component.TestAppComponent;
import com.rijatru.flickerclient.dependencyInjection.module.AppModule;
import com.rijatru.flickerclient.dependencyInjection.module.TestDataModule;
import com.rijatru.flickerclient.model.Photo;
import com.rijatru.flickerclient.model.data.interfaces.SerializationManager;
import com.rijatru.flickerclient.model.data.utils.TextUtil;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static android.support.test.InstrumentationRegistry.getInstrumentation;

public class UtilTest {

    private App app;

    public UtilTest() {
        super();
    }

    @Before
    public void setUp() throws Exception {
        System.setProperty("dexmaker.dexcache", getInstrumentation().getTargetContext().getCacheDir().getPath());

        TestAppComponent testComponent = DaggerTestAppComponent.builder()
                .appModule(new AppModule((App) getInstrumentation().getTargetContext().getApplicationContext()))
                .testDataModule(new TestDataModule())
                .build();

        testComponent.inject(this);

        app = ((FlickerApp) getInstrumentation().getTargetContext().getApplicationContext());
    }

    @Test
    public void testTextUtil() {
        String text = TextUtil.stripAccents("Bogotá");
        String testText = "Bogota";
        Assert.assertEquals(text, testText);
    }

    @Test
    public void testGetString() {
        String text = TextUtil.getString(app, R.string.app_name);
        Assert.assertFalse(TextUtils.isEmpty(text));
    }

    @Test
    public void testFormattedGetString() {
        String text = TextUtil.getString(app, R.string.photos_list_skip_offset, 1, 2);
        Assert.assertEquals(text, "1,2");
    }
}
