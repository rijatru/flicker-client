package com.rijatru.flickerclient.dependencyInjection.component;

import com.rijatru.flickerclient.ApiTest;
import com.rijatru.flickerclient.SerializationTest;
import com.rijatru.flickerclient.UtilTest;
import com.rijatru.flickerclient.dependencyInjection.module.AppModule;
import com.rijatru.flickerclient.dependencyInjection.module.TestDataModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, TestDataModule.class})
public interface TestAppComponent {

    void inject(SerializationTest mainActivityTest);

    void inject(UtilTest mainActivityTest);

    void inject(ApiTest apiTest);
}
