package com.rijatru.flickerclient.dependencyInjection.module;

import com.rijatru.flickerclient.App;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class TestAppModule {

    private final App app;

    public TestAppModule(App app) {
        this.app = app;
    }

    @Provides
    @Singleton
    public App app() {
        return app;
    }
}
