package com.rijatru.flickerclient.dependencyInjection.module;

import com.rijatru.flickerclient.App;
import com.rijatru.flickerclient.model.FlickerPhotosManager;
import com.rijatru.flickerclient.model.data.AndroidConnectivityManager;
import com.rijatru.flickerclient.model.data.AppPersistenceManager;
import com.rijatru.flickerclient.model.data.ConnectivityReceiver;
import com.rijatru.flickerclient.model.data.FlickerServiceManager;
import com.rijatru.flickerclient.model.data.GsonManager;
import com.rijatru.flickerclient.model.data.LogManager;
import com.rijatru.flickerclient.model.data.PhotosApiManager;
import com.rijatru.flickerclient.model.data.RetrofitManager;
import com.rijatru.flickerclient.model.data.RxBus;
import com.rijatru.flickerclient.model.data.interfaces.ApiManager;
import com.rijatru.flickerclient.model.data.interfaces.AppConnectivityManager;
import com.rijatru.flickerclient.model.data.interfaces.AppConnectivityReceiver;
import com.rijatru.flickerclient.model.data.interfaces.Bus;
import com.rijatru.flickerclient.model.data.interfaces.PersistenceManager;
import com.rijatru.flickerclient.model.data.interfaces.PhotosManager;
import com.rijatru.flickerclient.model.data.interfaces.RestManager;
import com.rijatru.flickerclient.model.data.interfaces.SerializationManager;
import com.rijatru.flickerclient.model.data.interfaces.ServiceManager;
import com.rijatru.flickerclient.model.data.persistence.DbHelper;
import com.rijatru.flickerclient.model.data.services.flickerApi.FlickerServices;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class TestDataModule {

    @Singleton
    @Provides
    public ApiManager apiManager(App app) {
        return new PhotosApiManager(app);
    }

    @Singleton
    @Provides
    public RestManager restManager(ApiManager apiManager) {
        return new RetrofitManager(apiManager);
    }

    @Singleton
    @Provides
    public ServiceManager serviceManager(FlickerServices photosApi, ApiManager apiManager, LogManager logManager, PersistenceManager persistenceManager, AppConnectivityManager appConnectivityManager) {
        return new FlickerServiceManager(photosApi, apiManager, logManager, persistenceManager, appConnectivityManager);
    }

    @Singleton
    @Provides
    public LogManager logManager() {
        return new LogManager(true);
    }

    @Singleton
    @Provides
    public FlickerServices photosApi(RestManager restManager) {
        return restManager.getPhotosRestClient().create(FlickerServices.class);
    }

    @Singleton
    @Provides
    public DbHelper dbHelper(App app, LogManager logManager, SerializationManager serializationManager) {
        return new DbHelper(app, logManager, serializationManager);
    }

    @Singleton
    @Provides
    public PersistenceManager persistenceManager(DbHelper dbHelper, LogManager logManager) {
        return new AppPersistenceManager(dbHelper, logManager);
    }

    @Singleton
    @Provides
    public SerializationManager serializationManager() {
        return new GsonManager<>();
    }

    @Singleton
    @Provides
    public AppConnectivityReceiver connectivityReceiver(App app) {
        return new ConnectivityReceiver(app);
    }

    @Singleton
    @Provides
    public AppConnectivityManager connectivityManager() {
        return new AndroidConnectivityManager();
    }

    @Singleton
    @Provides
    public Bus bud() {
        return new RxBus();
    }

    @Singleton
    @Provides
    public PhotosManager photosManager(PersistenceManager persistenceManager, ServiceManager serviceManager, LogManager logManager, AppConnectivityManager connectivityManager) {
        return new FlickerPhotosManager(persistenceManager, serviceManager, logManager, connectivityManager);
    }
}
