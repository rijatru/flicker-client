package com.rijatru.flickerclient.model.data;

import com.rijatru.flickerclient.model.Photo;
import com.rijatru.flickerclient.model.data.interfaces.ApiManager;
import com.rijatru.flickerclient.model.data.interfaces.AppConnectivityManager;
import com.rijatru.flickerclient.model.data.interfaces.PersistenceManager;
import com.rijatru.flickerclient.model.data.interfaces.PhotosServiceListener;
import com.rijatru.flickerclient.model.data.interfaces.ServiceManager;
import com.rijatru.flickerclient.model.data.services.flickerApi.FlickerServices;
import com.rijatru.flickerclient.model.data.services.flickerApi.model.getRecent.GetPhotosResponse;
import com.rijatru.flickerclient.model.data.utils.MapperUtil;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class TestFlickerServiceManager implements ServiceManager {

    @Inject
    public TestFlickerServiceManager() {
    }

    @Override
    public void getRecentPhotos(PhotosServiceListener listener) {

        List<Photo> photos = new ArrayList<>();

        Photo photo = new Photo();
        photo.setId(34244352);

        photos.add(photo);

        listener.onPhotos(photos);
    }

    @Override
    public void searchPhotos(int page, String searchTerm, PhotosServiceListener listener) {


    }
}
