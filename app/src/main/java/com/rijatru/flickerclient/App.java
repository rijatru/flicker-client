package com.rijatru.flickerclient;

import android.content.Context;

import com.rijatru.flickerclient.dependencyInjection.component.AppComponent;

public interface App {

    AppComponent getAppComponent();

    Context getApplicationContext();
}
