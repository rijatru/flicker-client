package com.rijatru.flickerclient;

import android.app.Application;

import com.rijatru.flickerclient.dependencyInjection.component.AppComponent;
import com.rijatru.flickerclient.dependencyInjection.component.DaggerAppComponent;
import com.rijatru.flickerclient.dependencyInjection.module.AppModule;
import com.rijatru.flickerclient.dependencyInjection.module.DataModule;
import com.rijatru.flickerclient.dependencyInjection.module.ViewsModule;

public class FlickerApp extends Application implements App {

    private static App app;
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (app == null) {
            app = this;
        }

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .dataModule(new DataModule())
                .viewsModule(new ViewsModule())
                .build();
    }

    public static App getApp() {
        return app;
    }

    @Override
    public AppComponent getAppComponent() {
        return appComponent;
    }
}
