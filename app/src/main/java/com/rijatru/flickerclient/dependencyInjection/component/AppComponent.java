package com.rijatru.flickerclient.dependencyInjection.component;

import com.rijatru.flickerclient.App;
import com.rijatru.flickerclient.dependencyInjection.module.AppModule;
import com.rijatru.flickerclient.dependencyInjection.module.DataModule;
import com.rijatru.flickerclient.dependencyInjection.module.ViewsModule;
import com.rijatru.flickerclient.model.data.LogManager;
import com.rijatru.flickerclient.model.data.interfaces.ApiManager;
import com.rijatru.flickerclient.model.data.interfaces.AppConnectivityManager;
import com.rijatru.flickerclient.model.data.interfaces.AppConnectivityReceiver;
import com.rijatru.flickerclient.model.data.interfaces.Bus;
import com.rijatru.flickerclient.model.data.interfaces.PersistenceManager;
import com.rijatru.flickerclient.model.data.interfaces.PhotosManager;
import com.rijatru.flickerclient.model.data.interfaces.SerializationManager;
import com.rijatru.flickerclient.model.data.interfaces.ServiceManager;
import com.rijatru.flickerclient.model.data.persistence.DbHelper;
import com.rijatru.flickerclient.view.factories.interfaces.ViewFactory;
import com.rijatru.flickerclient.viewModel.factories.interfaces.ListAdapterFactory;
import com.rijatru.flickerclient.viewModel.factories.interfaces.ViewModelFactory;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, DataModule.class, ViewsModule.class})
public interface AppComponent {

    App app();

    AppConnectivityReceiver connectivityReceiver();

    AppConnectivityManager connectivityManager();

    ServiceManager serviceManager();

    ApiManager apiManger();

    LogManager logManager();

    DbHelper dbHelper();

    PersistenceManager persistenceManager();

    SerializationManager serializationManager();

    ViewFactory viewFactory();

    ViewModelFactory viewModeFactory();

    ListAdapterFactory listAdapterFactory();

    Bus bus();

    PhotosManager photosManager();
}
