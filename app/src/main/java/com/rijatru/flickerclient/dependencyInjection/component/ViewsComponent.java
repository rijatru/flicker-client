package com.rijatru.flickerclient.dependencyInjection.component;

import com.rijatru.flickerclient.dependencyInjection.scope.AppScope;
import com.rijatru.flickerclient.view.BaseActivity;
import com.rijatru.flickerclient.view.BaseFragment;
import com.rijatru.flickerclient.view.MainActivity;
import com.rijatru.flickerclient.view.PhotoDetailActivity;
import com.rijatru.flickerclient.view.PhotoDetailFragment;
import com.rijatru.flickerclient.view.PhotoListItemView;
import com.rijatru.flickerclient.view.PhotoSearchFragment;
import com.rijatru.flickerclient.view.PhotosFragment;
import com.rijatru.flickerclient.view.SearchActivity;

import dagger.Component;

@AppScope
@Component(dependencies = AppComponent.class)
public interface ViewsComponent extends AppComponent {

    void inject(BaseActivity mainActivity);

    void inject(MainActivity mainActivity);

    void inject(BaseFragment photosView);

    void inject(PhotosFragment photosView);

    void inject(PhotoListItemView photosView);

    void inject(PhotoDetailFragment photosView);

    void inject(PhotoDetailActivity photoDetailActivity);

    void inject(SearchActivity photoDetailActivity);

    void inject(PhotoSearchFragment photoDetailActivity);
}
