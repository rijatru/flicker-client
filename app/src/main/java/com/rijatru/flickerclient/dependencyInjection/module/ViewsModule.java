package com.rijatru.flickerclient.dependencyInjection.module;

import com.rijatru.flickerclient.App;
import com.rijatru.flickerclient.model.data.LogManager;
import com.rijatru.flickerclient.model.data.interfaces.Bus;
import com.rijatru.flickerclient.model.data.interfaces.PersistenceManager;
import com.rijatru.flickerclient.model.data.interfaces.PhotosManager;
import com.rijatru.flickerclient.model.data.interfaces.ServiceManager;
import com.rijatru.flickerclient.view.factories.AppViewFactory;
import com.rijatru.flickerclient.view.factories.interfaces.ViewFactory;
import com.rijatru.flickerclient.viewModel.factories.AppListAdapterFactory;
import com.rijatru.flickerclient.viewModel.factories.AppViewModelFactory;
import com.rijatru.flickerclient.viewModel.factories.interfaces.ListAdapterFactory;
import com.rijatru.flickerclient.viewModel.factories.interfaces.ViewModelFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ViewsModule {

    @Singleton
    @Provides
    public ViewFactory viewFactory(App app) {
        return new AppViewFactory(app);
    }

    @Singleton
    @Provides
    public ListAdapterFactory listAdapterFactory(ViewFactory viewFactory, LogManager logManager) {
        return new AppListAdapterFactory(viewFactory, logManager);
    }

    @Singleton
    @Provides
    public ViewModelFactory viewModelFactory(App app, ServiceManager serviceManager, PersistenceManager persistenceManager, ListAdapterFactory listAdapterFactory, LogManager logManager, Bus bus, PhotosManager photosManager) {
        return new AppViewModelFactory(app, serviceManager, persistenceManager, listAdapterFactory, logManager, bus, photosManager);
    }
}
