package com.rijatru.flickerclient.model;

import android.support.v4.util.LongSparseArray;

import com.rijatru.flickerclient.model.data.LogManager;
import com.rijatru.flickerclient.model.data.interfaces.AppConnectivityManager;
import com.rijatru.flickerclient.model.data.interfaces.PersistenceManager;
import com.rijatru.flickerclient.model.data.interfaces.PhotosManager;
import com.rijatru.flickerclient.model.data.interfaces.ServiceManager;
import com.rijatru.flickerclient.model.data.persistence.adapters.PersistenceManagerCallbackAdapter;
import com.rijatru.flickerclient.model.data.utils.CollectionsUtil;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;

public class FlickerPhotosManager implements PhotosManager {

    private final PersistenceManager persistenceManager;
    private final ServiceManager serviceManager;
    private final LogManager logManager;
    private final AppConnectivityManager connectivityManager;

    private int currentPage = 1;

    private LongSparseArray<Photo> photosArray;
    private LongSparseArray<Photo> photosSearchArray;
    private PublishSubject<List<Photo>> photosSubject = PublishSubject.create();
    private BehaviorSubject<List<Photo>> searchSubject = BehaviorSubject.create();
    private PublishSubject<List<Photo>> newPhotosSubject = PublishSubject.create();

    @Inject
    public FlickerPhotosManager(PersistenceManager persistenceManager, ServiceManager serviceManager, LogManager logManager, AppConnectivityManager connectivityManager) {

        this.persistenceManager = persistenceManager;
        this.serviceManager = serviceManager;
        this.logManager = logManager;
        this.connectivityManager = connectivityManager;

        initConnectivityListener();
    }

    private void initConnectivityListener() {

        connectivityManager.getObservable()
                .doOnNext(isConnected -> {

                    if (photosArray == null) {
                        photosArray = new LongSparseArray<>();
                    }

                    if (isConnected && photosArray.size() == 0) {

                        getNewPhotos();
                    }
                })
                .doOnComplete(() -> logManager.log(getClass().getName(), "connectivityManager onComplete"))
                .subscribe();
    }

    @Override
    public void getPhotos() {

        if (photosArray == null) {

            persistenceManager.getPhotos(0, 24, 0, new PersistenceManagerCallbackAdapter() {

                @Override
                public void onPhotos(List<Photo> photos) {

                    addPhotosToCache(photos);
                    logManager.log(getClass().getName(), "Got Photos form persistence -> " + photos.size());
                    photosSubject.onNext(CollectionsUtil.longSparseArrayAsList(photosArray));
                }
            });

        } else {
            photosSubject.onNext(CollectionsUtil.longSparseArrayAsList(photosArray));
        }
    }

    @Override
    public void getMorePhotos() {

        int offset = photosArray.size() > 0 ? photosArray.size() - 1 : 0;

        persistenceManager.getPhotos(offset, 24, 750, new PersistenceManagerCallbackAdapter() {

            @Override
            public void onPhotos(List<Photo> photos) {

                logManager.log(getClass().getName(), "Got more Photos form persistence -> offset: " + (photosArray.size() - 1) + " " + photos.size());

                addPhotosToCache(photos);

                photosSubject.onNext(photos);
            }
        });
    }

    @Override
    public void getNewPhotos() {

        serviceManager.getRecentPhotos(newPhotos -> {

            List<Photo> photos = addPhotosToCache(newPhotos);
            logManager.log(getClass().getName(), "Got new Photos from service -> " + photos.size());
            newPhotosSubject.onNext(photos);
        });
    }

    @Override
    public Photo getPhotoById(long photoId) {

        Photo photo = photosArray.get(photoId);

        if (photo == null) {
            photo = photosSearchArray.get(photoId);
        }

        return photo;
    }

    @Override
    public void searchPhotos(String searchTerm) {

        currentPage = 1;

        serviceManager.searchPhotos(currentPage, searchTerm, searchResult -> {

            List<Photo> photos = addSearchPhotosToCache(searchResult);
            logManager.log(getClass().getName(), "Got new Photos from search service -> " + photos.size());
            searchSubject.onNext(photos);
        });
    }

    @Override
    public void clearSearchResults() {
        currentPage = 1;
        searchSubject.onNext(new ArrayList<>());
    }

    @Override
    public void resetSearchResults() {
        currentPage = 1;
        photosSearchArray = new LongSparseArray<>();
    }

    @Override
    public void searchMorePhotos(String searchTerm) {

        currentPage++;

        serviceManager.searchPhotos(currentPage, searchTerm, searchResult -> {

            List<Photo> photos = addSearchPhotosToCache(searchResult);
            logManager.log(getClass().getName(), "Got new Photos from search service -> " + photos.size());
            searchSubject.onNext(photos);
        });
    }

    @Override
    public boolean isNotFirstPage() {
        return photosSearchArray != null && photosSearchArray.size() > 99;
    }

    private List<Photo> addPhotosToCache(List<Photo> photos) {

        List<Photo> newPhotos = new ArrayList<>();

        if (photosArray == null) {
            photosArray = new LongSparseArray<>();
        }

        for (Photo photo : photos) {

            if (photosArray.get(photo.getId()) == null) {
                photosArray.put(photo.getId(), photo);
                newPhotos.add(photo);
            }
        }

        return newPhotos;
    }

    private List<Photo> addSearchPhotosToCache(List<Photo> photos) {

        List<Photo> newPhotos = new ArrayList<>();

        if (photosSearchArray == null) {
            photosSearchArray = new LongSparseArray<>();
        }

        for (Photo photo : photos) {

            if (photosSearchArray.get(photo.getId()) == null) {
                photosSearchArray.put(photo.getId(), photo);
                newPhotos.add(photo);
            }
        }

        return newPhotos;
    }

    @Override
    public Observable<List<Photo>> getPhotosObservable() {
        return photosSubject;
    }

    @Override
    public Observable<List<Photo>> getSearchObservable() {
        return searchSubject;
    }

    @Override
    public Observable<List<Photo>> getNewPhotosObservable() {
        return newPhotosSubject;
    }
}
