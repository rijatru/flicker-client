package com.rijatru.flickerclient.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.rijatru.flickerclient.model.data.utils.TextUtil;

public class Photo implements Parcelable {

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel source) {
            return new Photo(source);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    private long id;
    private String owner;
    private String title;
    private PhotoInfo info;
    private String searchableString;
    private int farm;
    private String server;
    private String secret;

    public Photo() {
    }

    public Photo(Parcel in) {

        this.id = in.readLong();
        this.owner = in.readString();
        this.title = in.readString();
        this.info = in.readParcelable(PhotoInfo.class.getClassLoader());
        this.searchableString = in.readString();
        this.farm = in.readInt();
        this.server = in.readString();
        this.secret = in.readString();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setInfo(PhotoInfo info) {
        this.info = info;
    }

    public PhotoInfo getInfo() {
        return info;
    }

    public void setSearchableString(String searchableString) {
        this.searchableString = searchableString;
    }

    public String getSearchableString() {

        if (TextUtils.isEmpty(searchableString)) {
            searchableString = id + " " + TextUtil.stripAccents(owner.toLowerCase()) + " " +  TextUtil.stripAccents(title.toLowerCase());
        }

        return searchableString;
    }

    public int getFarm() {
        return farm;
    }

    public void setFarm(int farm) {
        this.farm = farm;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeLong(this.id);
        dest.writeString(this.owner);
        dest.writeString(this.title);
        dest.writeParcelable(this.info, flags);
        dest.writeString(this.searchableString);
        dest.writeInt(this.farm);
        dest.writeString(this.server);
        dest.writeString(this.secret);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
