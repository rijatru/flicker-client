package com.rijatru.flickerclient.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PhotoInfo implements Parcelable {

    public static final Creator<PhotoInfo> CREATOR = new Creator<PhotoInfo>() {
        @Override
        public PhotoInfo createFromParcel(Parcel source) {
            return new PhotoInfo(source);
        }

        @Override
        public PhotoInfo[] newArray(int size) {
            return new PhotoInfo[size];
        }
    };

    private long uploaded;
    private String username;
    private String realName;
    private String taken;
    private long views;

    public PhotoInfo() {
    }

    public PhotoInfo(Parcel in) {

        this.uploaded = in.readLong();
        this.username = in.readString();
        this.realName = in.readString();
        this.taken = in.readString();
        this.views = in.readLong();
    }

    public long getUploaded() {
        return uploaded;
    }

    public void setUploaded(long uploaded) {
        this.uploaded = uploaded;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getTaken() {
        return taken;
    }

    public void setTaken(String taken) {
        this.taken = taken;
    }

    public long getViews() {
        return views;
    }

    public void setViews(long views) {
        this.views = views;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeLong(this.uploaded);
        dest.writeString(this.username);
        dest.writeString(this.realName);
        dest.writeString(this.taken);
        dest.writeLong(this.views);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
