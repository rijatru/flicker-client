package com.rijatru.flickerclient.model.data;

import com.rijatru.flickerclient.model.data.interfaces.AppConnectivityManager;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

public class AndroidConnectivityManager implements AppConnectivityManager {

    private BehaviorSubject<Boolean> connectionChangeSubject;

    public AndroidConnectivityManager() {
        this.connectionChangeSubject = BehaviorSubject.create();
    }

    @Override
    public void notifyChange(boolean isConnected) {
        connectionChangeSubject.onNext(isConnected);
    }

    @Override
    public Observable<Boolean> getObservable() {
        return connectionChangeSubject;
    }
}
