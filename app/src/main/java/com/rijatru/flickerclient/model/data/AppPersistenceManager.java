package com.rijatru.flickerclient.model.data;

import com.rijatru.flickerclient.model.Photo;
import com.rijatru.flickerclient.model.data.interfaces.PersistenceManager;
import com.rijatru.flickerclient.model.data.interfaces.PersistenceManagerCallback;
import com.rijatru.flickerclient.model.data.persistence.DbHelper;
import com.rijatru.flickerclient.model.data.persistence.adapters.PersistenceManagerCallbackAdapter;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AppPersistenceManager implements PersistenceManager {

    private final DbHelper dbHelper;
    private final LogManager logManager;

    @Inject
    public AppPersistenceManager(DbHelper dbHelper, LogManager logManager) {
        this.dbHelper = dbHelper;
        this.logManager = logManager;
    }

    public void savePhotos(List<Photo> photos) {

       dbHelper.savePhotos(photos)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(() -> logManager.log(getClass().getName(), "Saved " + photos.size() + " photos"))
                .subscribe(savedPhoto -> {

                });
    }

    @Override
    public void getPhotos(int offset, int limit, int delay, PersistenceManagerCallbackAdapter callback) {

        dbHelper.getPhotos(offset, limit)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .delay(delay, TimeUnit.MILLISECONDS) // Simulate delay
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(() -> logManager.log(getClass().getName(), "Got Photos"))
                .subscribe(callback::onPhotos);
    }

    public void savePhoto(Photo photo) {

        dbHelper.savePhoto(photo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(() -> logManager.log(getClass().getName(), "Saved ResponsePhoto " + photo.getTitle()))
                .subscribe(savedPhoto -> {

                });
    }

    public void getMediaById(String id, PersistenceManagerCallback callback) {

       dbHelper.getPhotoById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(() -> logManager.log(getClass().getName(), "Got MediaById"))
                .subscribe(callback::onPhoto);
    }

    public void searchPhotosBy(String searchTerm, PersistenceManagerCallback callback) {

        dbHelper.searchPhotos(searchTerm)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(() -> logManager.log(getClass().getName(), "Got PhotosBy"))
                .subscribe(callback::onPhotos);
    }
}
