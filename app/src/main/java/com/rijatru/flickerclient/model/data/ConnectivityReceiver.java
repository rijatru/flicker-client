package com.rijatru.flickerclient.model.data;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.rijatru.flickerclient.App;
import com.rijatru.flickerclient.model.data.interfaces.AppConnectivityReceiver;
import com.rijatru.flickerclient.model.data.utils.NetworkUtil;

import javax.inject.Inject;

public class ConnectivityReceiver extends BroadcastReceiver implements AppConnectivityReceiver {

    private ConnectivityReceiverListener listener;
    private App app;

    @Inject
    public ConnectivityReceiver(App app) {
        this.app = app;
    }

    @Override
    public BroadcastReceiver getReceiver() {
        return this;
    }

    public void setListener(ConnectivityReceiverListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        returnStatus();
    }

    private void returnStatus() {

        if (listener != null) {
            listener.onConnectivityChange(NetworkUtil.isNetworkAvailable(app.getApplicationContext()));
        }
    }

    public interface ConnectivityReceiverListener {

        void onConnectivityChange(boolean isConnected);
    }
}
