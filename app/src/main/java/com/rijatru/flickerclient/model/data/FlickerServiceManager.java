package com.rijatru.flickerclient.model.data;

import com.rijatru.flickerclient.model.Photo;
import com.rijatru.flickerclient.model.data.interfaces.ApiManager;
import com.rijatru.flickerclient.model.data.interfaces.AppConnectivityManager;
import com.rijatru.flickerclient.model.data.interfaces.PersistenceManager;
import com.rijatru.flickerclient.model.data.interfaces.PhotosServiceListener;
import com.rijatru.flickerclient.model.data.interfaces.ServiceManager;
import com.rijatru.flickerclient.model.data.services.flickerApi.FlickerServices;
import com.rijatru.flickerclient.model.data.services.flickerApi.model.getRecent.GetPhotosResponse;
import com.rijatru.flickerclient.model.data.utils.MapperUtil;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class FlickerServiceManager implements ServiceManager {

    private final FlickerServices photosApi;
    private final ApiManager apiManager;
    private final LogManager logManager;
    private final PersistenceManager persistenceManager;
    private final AppConnectivityManager connectivityManager;

    @Inject
    public FlickerServiceManager(FlickerServices photosApi, ApiManager apiManager, LogManager logManager, PersistenceManager persistenceManager, AppConnectivityManager connectivityManager) {
        this.photosApi = photosApi;
        this.apiManager = apiManager;
        this.logManager = logManager;
        this.persistenceManager = persistenceManager;
        this.connectivityManager = connectivityManager;
    }

    @Override
    public void getRecentPhotos(PhotosServiceListener listener) {

        photosApi.getRecent(apiManager.getPhotosApiKey())
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .doOnError(error -> logManager.log(getClass().getName(), error))
                .onErrorReturn(this::handleThrowable)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((photosResponse) -> handleResponse(photosResponse, listener));
    }

    @Override
    public void searchPhotos(int page, String searchTerm, PhotosServiceListener listener) {

        photosApi.searchPhotos(apiManager.getPhotosApiKey(), searchTerm.trim().replaceAll("\\s+", "+"), page)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .doOnError(error -> logManager.log(getClass().getName(), error))
                .onErrorReturn(this::handleThrowable)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((photosResponse) -> handleSearchResponse(photosResponse, listener));
    }

    private void handleResponse(GetPhotosResponse photosResponse, PhotosServiceListener listener) {

        List<Photo> photos = MapperUtil.getPhotoObjects(photosResponse);
        persistenceManager.savePhotos(photos);
        listener.onPhotos(photos);
    }

    private void handleSearchResponse(GetPhotosResponse photosResponse, PhotosServiceListener listener) {

        List<Photo> photos = MapperUtil.getSearchPhotoObjects(photosResponse);
        listener.onPhotos(photos);
    }

    private GetPhotosResponse handleThrowable(Throwable throwable) {
        logManager.log(getClass().getName(), throwable);
        return new GetPhotosResponse();
    }
}
