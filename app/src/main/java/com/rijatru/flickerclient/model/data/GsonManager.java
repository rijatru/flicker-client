package com.rijatru.flickerclient.model.data;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rijatru.flickerclient.model.data.interfaces.SerializationManager;

import java.lang.reflect.Type;
import java.util.List;

public class GsonManager<T> implements SerializationManager {

    @Override
    public String getJson(Object object) {

        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .serializeNulls()
                .create();

        return gson.toJson(object);
    }

    @Override
    public Object fromJson(String objectString, Class classType) {

        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .serializeNulls()
                .create();

        return gson.fromJson(objectString, classType);
    }

    public Object fromJson(String objectString, Type classType) {

        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .serializeNulls()
                .create();

        return gson.fromJson(objectString, classType);
    }

    public String getJsonArray(Object object, Type listOfTestObject) {

        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .serializeNulls()
                .create();

        return gson.toJson(object, listOfTestObject);
    }

    public List<T> fromJsonArray(String string, Type listOfTestObject) {

        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .serializeNulls()
                .create();

        return gson.fromJson(string, listOfTestObject);
    }
}
