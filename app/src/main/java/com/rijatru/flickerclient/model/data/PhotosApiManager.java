package com.rijatru.flickerclient.model.data;

import com.rijatru.flickerclient.App;
import com.rijatru.flickerclient.R;
import com.rijatru.flickerclient.model.data.interfaces.ApiManager;
import com.rijatru.flickerclient.model.data.utils.TextUtil;

import javax.inject.Inject;

public class PhotosApiManager implements ApiManager {

    private final App app;

    @Inject
    public PhotosApiManager(App app) {
        this.app = app;
    }

    @Override
    public String getPhotosApiUrl() {
        return TextUtil.getString(app, R.string.flicker_api_url);
    }

    @Override
    public String getPhotosApiKey() {
        return TextUtil.getString(app, R.string.flicker_api_key);
    }
}
