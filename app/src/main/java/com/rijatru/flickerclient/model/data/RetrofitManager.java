package com.rijatru.flickerclient.model.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rijatru.flickerclient.model.data.interfaces.ApiManager;
import com.rijatru.flickerclient.model.data.interfaces.RestManager;
import com.rijatru.flickerclient.model.data.serialization.InternalIdModelExclusionStrategy;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitManager implements RestManager {

    private final ApiManager apiManager;

    @Inject
    public RetrofitManager(ApiManager apiManager) {
        this.apiManager = apiManager;
    }

    public retrofit2.Retrofit getPhotosRestClient() {

        Gson gson = new GsonBuilder()
                .addSerializationExclusionStrategy(new InternalIdModelExclusionStrategy())
                .create();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        return new retrofit2.Retrofit.Builder()
                .baseUrl(apiManager.getPhotosApiUrl())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient.build())
                .build();
    }
}
