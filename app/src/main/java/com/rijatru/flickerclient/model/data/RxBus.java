package com.rijatru.flickerclient.model.data;

import android.os.Bundle;

import com.rijatru.flickerclient.model.data.interfaces.Bus;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class RxBus implements Bus {

    public static final String PHOTO = "photo";
    public static final String CODE = "code";

    public static final int OPEN_PHOTO = 100;

    private PublishSubject<Bundle> publishSubject = PublishSubject.create();

    @Override
    public void publish(Bundle bundle) {
        publishSubject.onNext(bundle);
    }

    @Override
    public Observable<Bundle> getObservable() {
        return publishSubject;
    }
}
