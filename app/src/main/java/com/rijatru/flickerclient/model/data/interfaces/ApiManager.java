package com.rijatru.flickerclient.model.data.interfaces;

public interface ApiManager {

    String getPhotosApiUrl();

    String getPhotosApiKey();
}
