package com.rijatru.flickerclient.model.data.interfaces;

import io.reactivex.Observable;

public interface AppConnectivityManager {

    void notifyChange(boolean isConnected);

    Observable<Boolean> getObservable();
}
