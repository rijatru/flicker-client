package com.rijatru.flickerclient.model.data.interfaces;

import android.content.BroadcastReceiver;

import com.rijatru.flickerclient.model.data.ConnectivityReceiver;

public interface AppConnectivityReceiver {

    BroadcastReceiver getReceiver();

    void setListener(ConnectivityReceiver.ConnectivityReceiverListener listener);
}
