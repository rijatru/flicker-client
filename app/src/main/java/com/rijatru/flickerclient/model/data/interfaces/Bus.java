package com.rijatru.flickerclient.model.data.interfaces;

import android.os.Bundle;

import io.reactivex.Observable;

public interface Bus {

    void publish(Bundle bundle);

    Observable<Bundle> getObservable();
}
