package com.rijatru.flickerclient.model.data.interfaces;

import com.rijatru.flickerclient.model.Photo;
import com.rijatru.flickerclient.model.data.persistence.adapters.PersistenceManagerCallbackAdapter;

import java.util.List;

public interface PersistenceManager {

    void savePhotos(List<Photo> photos);

    void getPhotos(int offset, int limit, int delay, PersistenceManagerCallbackAdapter persistenceManagerCallbackAdapter);
}
