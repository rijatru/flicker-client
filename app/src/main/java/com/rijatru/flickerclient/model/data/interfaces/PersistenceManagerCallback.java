package com.rijatru.flickerclient.model.data.interfaces;

import com.rijatru.flickerclient.model.Photo;

import java.util.List;

public interface PersistenceManagerCallback {

    void onPhotos(List<Photo> photos);

    void onPhoto(Photo photo);
}
