package com.rijatru.flickerclient.model.data.interfaces;

import com.rijatru.flickerclient.model.Photo;

import java.util.List;

import io.reactivex.Observable;

public interface PhotosManager {

    Observable<List<Photo>> getPhotosObservable();

    Observable<List<Photo>> getSearchObservable();

    Observable<List<Photo>> getNewPhotosObservable();

    void getPhotos();

    void getMorePhotos();

    void getNewPhotos();

    Photo getPhotoById(long photoId);

    void searchPhotos(String searchTerm);

    void clearSearchResults();

    void resetSearchResults();

    void searchMorePhotos(String searchTerm);

    boolean isNotFirstPage();
}
