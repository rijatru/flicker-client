package com.rijatru.flickerclient.model.data.interfaces;

import com.rijatru.flickerclient.model.Photo;

import java.util.List;

public interface PhotosServiceListener {

    void onPhotos(List<Photo> photos);
}
