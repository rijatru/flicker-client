package com.rijatru.flickerclient.model.data.interfaces;

import retrofit2.Retrofit;

public interface RestManager {

    Retrofit getPhotosRestClient();
}
