package com.rijatru.flickerclient.model.data.interfaces;

public interface SerializationManager {

    String getJson(Object value);

    Object fromJson(String infoString, Class photoInfoClass);
}
