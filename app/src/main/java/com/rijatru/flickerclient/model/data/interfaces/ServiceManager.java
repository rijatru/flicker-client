package com.rijatru.flickerclient.model.data.interfaces;

public interface ServiceManager {

    void getRecentPhotos(PhotosServiceListener listener);

    void searchPhotos(int page, String searchTerm, PhotosServiceListener listener);
}
