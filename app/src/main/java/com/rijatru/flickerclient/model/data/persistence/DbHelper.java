package com.rijatru.flickerclient.model.data.persistence;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.rijatru.flickerclient.App;
import com.rijatru.flickerclient.R;
import com.rijatru.flickerclient.model.Photo;
import com.rijatru.flickerclient.model.PhotoInfo;
import com.rijatru.flickerclient.model.data.LogManager;
import com.rijatru.flickerclient.model.data.interfaces.SerializationManager;
import com.rijatru.flickerclient.model.data.utils.TextUtil;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class DbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 8;

    public static final String DATABASE_NAME = "ResponsePhoto.db";

    private static final String SQL_CREATE_PHOTOS_TABLE =
            "CREATE TABLE " + PhotoContract.PhotoEntry.TABLE_NAME + " (" +
                    PhotoContract.PhotoEntry.COLUMN_NAME_ID + " LONG UNIQUE PRIMARY KEY," +
                    PhotoContract.PhotoEntry.COLUMN_NAME_OWNER + " TEXT," +
                    PhotoContract.PhotoEntry.COLUMN_NAME_TITLE + " TEXT," +
                    PhotoContract.PhotoEntry.COLUMN_NAME_FARM + " INTEGER," +
                    PhotoContract.PhotoEntry.COLUMN_NAME_SERVER + " TEXT," +
                    PhotoContract.PhotoEntry.COLUMN_NAME_SECRET + " TEXT," +
                    PhotoContract.PhotoEntry.COLUMN_NAME_INFO + " TEXT," +
                    PhotoContract.PhotoEntry.COLUMN_NAME_SEARCH + " TEXT)";

    private static final String SQL_DELETE_PHOTOS = "DROP TABLE IF EXISTS " + PhotoContract.PhotoEntry.TABLE_NAME;

    private final App app;
    private final SerializationManager serializationManager;
    private final LogManager logManager;

    @Inject
    public DbHelper(App app, LogManager logManager, SerializationManager serializationManager) {
        super(app.getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);

        this.app = app;
        this.serializationManager = serializationManager;
        this.logManager = logManager;
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_PHOTOS_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(SQL_DELETE_PHOTOS);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public Observable<List<Photo>> savePhotos(List<Photo> photos) {

        return Observable.create(subscriber -> {

            SQLiteDatabase db = getWritableDatabase();
            db.beginTransaction();

            try {

                ContentValues values = new ContentValues();

                for (Photo photo : photos) {

                    values.put(PhotoContract.PhotoEntry.COLUMN_NAME_ID, photo.getId());
                    values.put(PhotoContract.PhotoEntry.COLUMN_NAME_OWNER, photo.getOwner());
                    values.put(PhotoContract.PhotoEntry.COLUMN_NAME_TITLE, photo.getTitle());
                    values.put(PhotoContract.PhotoEntry.COLUMN_NAME_FARM, photo.getFarm());
                    values.put(PhotoContract.PhotoEntry.COLUMN_NAME_SERVER, photo.getServer());
                    values.put(PhotoContract.PhotoEntry.COLUMN_NAME_SECRET, photo.getSecret());
                    values.put(PhotoContract.PhotoEntry.COLUMN_NAME_INFO, serializationManager.getJson(photo.getInfo()));
                    values.put(PhotoContract.PhotoEntry.COLUMN_NAME_SEARCH, photo.getSearchableString());

                    db.insertWithOnConflict(PhotoContract.PhotoEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);

                    logManager.log(getClass().getName(), "Saved photo in DB -> " + photo.getId());
                }

                db.setTransactionSuccessful();

                subscriber.onNext(photos);
                subscriber.onComplete();

            } catch (Exception ex) {
                subscriber.onError(ex);
            } finally {
                db.endTransaction();
            }
        });
    }

    public Observable<Photo> savePhoto(Photo photo) {

        return Observable.create(subscriber -> {

            SQLiteDatabase db = getWritableDatabase();
            db.beginTransaction();

            try {

                ContentValues values = new ContentValues();

                values.put(PhotoContract.PhotoEntry.COLUMN_NAME_ID, photo.getId());
                values.put(PhotoContract.PhotoEntry.COLUMN_NAME_OWNER, photo.getOwner());
                values.put(PhotoContract.PhotoEntry.COLUMN_NAME_TITLE, photo.getTitle());
                values.put(PhotoContract.PhotoEntry.COLUMN_NAME_FARM, photo.getFarm());
                values.put(PhotoContract.PhotoEntry.COLUMN_NAME_SERVER, photo.getServer());
                values.put(PhotoContract.PhotoEntry.COLUMN_NAME_SECRET, photo.getSecret());
                values.put(PhotoContract.PhotoEntry.COLUMN_NAME_INFO, serializationManager.getJson(photo.getInfo()));
                values.put(PhotoContract.PhotoEntry.COLUMN_NAME_SEARCH, photo.getSearchableString());

                db.insertWithOnConflict(PhotoContract.PhotoEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                db.setTransactionSuccessful();

                logManager.log(getClass().getName(), "Saved photo in DB -> " + photo.getId());

                subscriber.onNext(photo);
                subscriber.onComplete();

            } catch (Exception ex) {
                subscriber.onError(ex);
            } finally {
                db.endTransaction();
            }
        });
    }

    public Observable<List<Photo>> getPhotos(int offset, int limit) {

        return Observable.create(subscriber -> {

            try {

                SQLiteDatabase db = getReadableDatabase();

                String[] projection = getPhotoProjection();
                String sortOrder = PhotoContract.PhotoEntry.COLUMN_NAME_ID + " DESC";
                String queryOffsetAndLimit = TextUtil.getString(app, R.string.photos_list_skip_offset, offset, limit);

                Cursor cursor = db.query(
                        PhotoContract.PhotoEntry.TABLE_NAME,
                        projection,
                        null,
                        null,
                        null,
                        null,
                        sortOrder,
                        queryOffsetAndLimit
                );

                List<Photo> photos = new ArrayList<>();

                while (cursor.moveToNext()) {
                    photos.add(getPhotoObject(cursor));
                }

                cursor.close();

                subscriber.onNext(photos);
                subscriber.onComplete();

            } catch (Exception ex) {

                subscriber.onError(ex);
            }
        });
    }

    public Observable<Photo> getPhotoById(String id) {

        return Observable.create(subscriber -> {

            try {

                SQLiteDatabase db = getReadableDatabase();

                String[] projection = getPhotoProjection();
                String sortOrder = PhotoContract.PhotoEntry.COLUMN_NAME_ID + " DESC";
                String selection = PhotoContract.PhotoEntry.COLUMN_NAME_ID + " =? ";
                String[] selectionArgs = {
                        id
                };

                Cursor cursor = db.query(
                        PhotoContract.PhotoEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );

                Photo photo = new Photo();

                while (cursor.moveToNext()) {
                    photo = getPhotoObject(cursor);
                }

                cursor.close();

                subscriber.onNext(photo);
                subscriber.onComplete();

            } catch (Exception ex) {

                subscriber.onError(ex);
            }
        });
    }

    public Observable<List<Photo>> searchPhotos(String searchTerm) {

        return Observable.create(subscriber -> {

            try {

                SQLiteDatabase db = getReadableDatabase();

                String selectQuery = " SELECT * FROM " + PhotoContract.PhotoEntry.TABLE_NAME + " WHERE " + PhotoContract.PhotoEntry.COLUMN_NAME_SEARCH + " LIKE ";
                String[] splited = searchTerm.split("\\s+");

                if (splited.length > 0) {
                    selectQuery += "'%" + splited[0] + "%'";
                }

                for (String word : splited) {
                    selectQuery += " OR " + PhotoContract.PhotoEntry.COLUMN_NAME_SEARCH + " LIKE '%" + word + "%'";
                }

                Cursor cursor = db.rawQuery(selectQuery, null);

                List<Photo> photos = new ArrayList<>();

                while (cursor.moveToNext()) {
                    photos.add(getPhotoObject(cursor));
                }

                cursor.close();

                subscriber.onNext(photos);
                subscriber.onComplete();

            } catch (Exception ex) {

                subscriber.onError(ex);
            }
        });
    }

    private Photo getPhotoObject(Cursor cursor) {

        Photo photo = new Photo();

        long id = cursor.getLong(cursor.getColumnIndexOrThrow(PhotoContract.PhotoEntry.COLUMN_NAME_ID));
        String owner = cursor.getString(cursor.getColumnIndexOrThrow(PhotoContract.PhotoEntry.COLUMN_NAME_OWNER));
        String title = cursor.getString(cursor.getColumnIndexOrThrow(PhotoContract.PhotoEntry.COLUMN_NAME_TITLE));
        int farm = cursor.getInt(cursor.getColumnIndexOrThrow(PhotoContract.PhotoEntry.COLUMN_NAME_FARM));
        String server = cursor.getString(cursor.getColumnIndexOrThrow(PhotoContract.PhotoEntry.COLUMN_NAME_SERVER));
        String secret = cursor.getString(cursor.getColumnIndexOrThrow(PhotoContract.PhotoEntry.COLUMN_NAME_SECRET));
        String searchableString = cursor.getString(cursor.getColumnIndexOrThrow(PhotoContract.PhotoEntry.COLUMN_NAME_SEARCH));

        String infoString = cursor.getString(cursor.getColumnIndexOrThrow(PhotoContract.PhotoEntry.COLUMN_NAME_INFO));
        PhotoInfo photoInfo = (PhotoInfo) serializationManager.fromJson(infoString, PhotoInfo.class);

        photo.setId(id);
        photo.setOwner(owner);
        photo.setTitle(title);
        photo.setFarm(farm);
        photo.setServer(server);
        photo.setSecret(secret);
        photo.setInfo(photoInfo);
        photo.setSearchableString(searchableString);

        return photo;
    }

    @NonNull
    private String[] getPhotoProjection() {
        return new String[]{
                PhotoContract.PhotoEntry.COLUMN_NAME_ID,
                PhotoContract.PhotoEntry.COLUMN_NAME_OWNER,
                PhotoContract.PhotoEntry.COLUMN_NAME_TITLE,
                PhotoContract.PhotoEntry.COLUMN_NAME_FARM,
                PhotoContract.PhotoEntry.COLUMN_NAME_SERVER,
                PhotoContract.PhotoEntry.COLUMN_NAME_SECRET,
                PhotoContract.PhotoEntry.COLUMN_NAME_INFO,
                PhotoContract.PhotoEntry.COLUMN_NAME_SEARCH
        };
    }
}
