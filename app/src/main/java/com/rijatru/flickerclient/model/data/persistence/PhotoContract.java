package com.rijatru.flickerclient.model.data.persistence;

import android.provider.BaseColumns;

public class PhotoContract implements BaseColumns {

    public static class PhotoEntry implements BaseColumns {

        public static final String TABLE_NAME = "Photos";
        public static final String COLUMN_NAME_ID = "photoId";
        public static final String COLUMN_NAME_OWNER = "photoOwner";
        public static final String COLUMN_NAME_TITLE = "photoTitle";
        public static final String COLUMN_NAME_FARM = "photoFarm";
        public static final String COLUMN_NAME_SERVER = "photoServer";
        public static final String COLUMN_NAME_SECRET = "photoSecret";
        public static final String COLUMN_NAME_INFO = "photoInfo";
        public static final String COLUMN_NAME_SEARCH = "photoSearch";
    }
}
