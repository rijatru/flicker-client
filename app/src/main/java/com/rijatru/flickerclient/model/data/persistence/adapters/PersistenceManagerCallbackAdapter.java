package com.rijatru.flickerclient.model.data.persistence.adapters;

import com.rijatru.flickerclient.model.Photo;
import com.rijatru.flickerclient.model.data.interfaces.PersistenceManagerCallback;

import java.util.List;

public class PersistenceManagerCallbackAdapter implements PersistenceManagerCallback {

    @Override
    public void onPhotos(List<Photo> photos) {

    }

    @Override
    public void onPhoto(Photo photo) {

    }
}
