package com.rijatru.flickerclient.model.data.services.flickerApi;

import com.rijatru.flickerclient.model.data.services.flickerApi.model.getRecent.GetPhotosResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FlickerServices {

    @GET("services/rest?method=flickr.photos.getRecent&format=json&nojsoncallback=1")
    Observable<GetPhotosResponse> getRecent(@Query("api_key") String apiKey);

    @GET("services/rest/?method=flickr.photos.search&format=json&nojsoncallback=1")
    Observable<GetPhotosResponse> searchPhotos(@Query("api_key") String apiKey, @Query("tags") String search, @Query("page") int page);
}
