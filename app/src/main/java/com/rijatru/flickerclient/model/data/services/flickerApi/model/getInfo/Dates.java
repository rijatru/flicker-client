package com.rijatru.flickerclient.model.data.services.flickerApi.model.getInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Dates {

    @SerializedName("posted")
    @Expose
    public String posted;
    @SerializedName("taken")
    @Expose
    public String taken;
    @SerializedName("takengranularity")
    @Expose
    public String takengranularity;
    @SerializedName("takenunknown")
    @Expose
    public String takenunknown;
    @SerializedName("lastupdate")
    @Expose
    public String lastupdate;
}
