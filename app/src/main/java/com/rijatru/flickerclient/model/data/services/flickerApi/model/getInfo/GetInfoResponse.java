package com.rijatru.flickerclient.model.data.services.flickerApi.model.getInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetInfoResponse {

    @SerializedName("photo")
    @Expose
    public ResponsePhoto photo;
    @SerializedName("stat")
    @Expose
    public String stat;
}
