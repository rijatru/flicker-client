package com.rijatru.flickerclient.model.data.services.flickerApi.model.getInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Notes {

    @SerializedName("note")
    @Expose
    public List<Object> note = null;
}
