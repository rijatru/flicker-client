package com.rijatru.flickerclient.model.data.services.flickerApi.model.getInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Owner {

    @SerializedName("nsid")
    @Expose
    public String nsid;
    @SerializedName("username")
    @Expose
    public String username;
    @SerializedName("realname")
    @Expose
    public String realname;
    @SerializedName("location")
    @Expose
    public String location;
    @SerializedName("iconserver")
    @Expose
    public String iconserver;
    @SerializedName("iconfarm")
    @Expose
    public long iconfarm;
    @SerializedName("path_alias")
    @Expose
    public Object pathAlias;
}
