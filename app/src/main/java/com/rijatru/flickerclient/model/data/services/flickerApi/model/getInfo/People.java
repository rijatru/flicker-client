package com.rijatru.flickerclient.model.data.services.flickerApi.model.getInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class People {

    @SerializedName("haspeople")
    @Expose
    public long haspeople;
}
