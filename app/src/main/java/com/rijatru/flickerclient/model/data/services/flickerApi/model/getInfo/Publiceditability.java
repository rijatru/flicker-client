package com.rijatru.flickerclient.model.data.services.flickerApi.model.getInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Publiceditability {

    @SerializedName("cancomment")
    @Expose
    public long cancomment;
    @SerializedName("canaddmeta")
    @Expose
    public long canaddmeta;
}
