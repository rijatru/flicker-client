package com.rijatru.flickerclient.model.data.services.flickerApi.model.getInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponsePhoto {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("secret")
    @Expose
    public String secret;
    @SerializedName("server")
    @Expose
    public String server;
    @SerializedName("farm")
    @Expose
    public long farm;
    @SerializedName("dateuploaded")
    @Expose
    public String dateuploaded;
    @SerializedName("isfavorite")
    @Expose
    public long isfavorite;
    @SerializedName("license")
    @Expose
    public String license;
    @SerializedName("safety_level")
    @Expose
    public String safetyLevel;
    @SerializedName("rotation")
    @Expose
    public long rotation;
    @SerializedName("originalsecret")
    @Expose
    public String originalsecret;
    @SerializedName("originalformat")
    @Expose
    public String originalformat;
    @SerializedName("owner")
    @Expose
    public Owner owner;
    @SerializedName("title")
    @Expose
    public Title title;
    @SerializedName("description")
    @Expose
    public Description description;
    @SerializedName("visibility")
    @Expose
    public Visibility visibility;
    @SerializedName("dates")
    @Expose
    public Dates dates;
    @SerializedName("views")
    @Expose
    public String views;
    @SerializedName("editability")
    @Expose
    public Editability editability;
    @SerializedName("publiceditability")
    @Expose
    public Publiceditability publiceditability;
    @SerializedName("usage")
    @Expose
    public Usage usage;
    @SerializedName("comments")
    @Expose
    public Comments comments;
    @SerializedName("notes")
    @Expose
    public Notes notes;
    @SerializedName("people")
    @Expose
    public People people;
    @SerializedName("tags")
    @Expose
    public Tags tags;
    @SerializedName("urls")
    @Expose
    public Urls urls;
    @SerializedName("media")
    @Expose
    public String media;
}
