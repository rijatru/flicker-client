package com.rijatru.flickerclient.model.data.services.flickerApi.model.getInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Usage {

    @SerializedName("candownload")
    @Expose
    public long candownload;
    @SerializedName("canblog")
    @Expose
    public long canblog;
    @SerializedName("canprint")
    @Expose
    public long canprint;
    @SerializedName("canshare")
    @Expose
    public long canshare;
}
