package com.rijatru.flickerclient.model.data.services.flickerApi.model.getInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Visibility {

    @SerializedName("ispublic")
    @Expose
    public long ispublic;
    @SerializedName("isfriend")
    @Expose
    public long isfriend;
    @SerializedName("isfamily")
    @Expose
    public long isfamily;
}
