package com.rijatru.flickerclient.model.data.services.flickerApi.model.getRecent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetPhotosResponse {

    @SerializedName("photos")
    @Expose
    public ResponsePhotos photos;
    @SerializedName("stat")
    @Expose
    public String stat;
}
