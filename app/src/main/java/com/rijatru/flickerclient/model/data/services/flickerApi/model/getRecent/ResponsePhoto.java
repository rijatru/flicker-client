package com.rijatru.flickerclient.model.data.services.flickerApi.model.getRecent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponsePhoto {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("owner")
    @Expose
    public String owner;
    @SerializedName("secret")
    @Expose
    public String secret;
    @SerializedName("server")
    @Expose
    public String server;
    @SerializedName("farm")
    @Expose
    public int farm;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("ispublic")
    @Expose
    public long ispublic;
    @SerializedName("isfriend")
    @Expose
    public long isfriend;
    @SerializedName("isfamily")
    @Expose
    public long isfamily;
}
