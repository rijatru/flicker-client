package com.rijatru.flickerclient.model.data.services.flickerApi.model.getRecent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponsePhotos {

    @SerializedName("page")
    @Expose
    public long page;
    @SerializedName("pages")
    @Expose
    public long pages;
    @SerializedName("perpage")
    @Expose
    public long perpage;
    @SerializedName("total")
    @Expose
    public long total;
    @SerializedName("photo")
    @Expose
    public List<ResponsePhoto> photos = null;
}
