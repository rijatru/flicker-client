package com.rijatru.flickerclient.model.data.utils;

import android.support.v4.util.LongSparseArray;

import java.util.ArrayList;
import java.util.List;

public class CollectionsUtil {

    public static <C> List<C> longSparseArrayAsList(LongSparseArray<C> sparseArray) {

        if (sparseArray == null) {
            return null;
        }

        List<C> arrayList = new ArrayList<>(sparseArray.size());

        for (int i = 0; i < sparseArray.size(); i++) {
            arrayList.add(sparseArray.valueAt(i));
        }

        return arrayList;
    }
}
