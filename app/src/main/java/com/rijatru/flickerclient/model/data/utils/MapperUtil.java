package com.rijatru.flickerclient.model.data.utils;

import android.util.Log;

import com.rijatru.flickerclient.App;
import com.rijatru.flickerclient.model.Photo;
import com.rijatru.flickerclient.model.PhotoListItem;
import com.rijatru.flickerclient.model.data.services.flickerApi.model.getRecent.GetPhotosResponse;
import com.rijatru.flickerclient.model.data.services.flickerApi.model.getRecent.ResponsePhoto;
import com.rijatru.flickerclient.model.interfaces.Item;
import com.rijatru.flickerclient.view.factories.AppViewFactory;

import java.util.ArrayList;
import java.util.List;

public final class MapperUtil {

    private MapperUtil() {
    }

    public static List<Photo> getPhotoObjects(GetPhotosResponse getRecentResponse) {

        List<Photo> photos = new ArrayList<>();

        try {

            for (ResponsePhoto responsePhoto : getRecentResponse.photos.photos) {

                Photo photo = new Photo();

                photo.setId(Long.parseLong(responsePhoto.id));
                photo.setOwner(responsePhoto.owner);
                photo.setTitle(responsePhoto.title);
                photo.setFarm(responsePhoto.farm);
                photo.setServer(responsePhoto.server);
                photo.setSecret(responsePhoto.secret);

                photos.add(photo);
            }

        } catch (Exception exception) {
            Log.e("getPhotoObjects", exception.getMessage());
        }

        return photos;
    }

    public static List<Photo> getSearchPhotoObjects(GetPhotosResponse getRecentResponse) {

        List<Photo> photos = new ArrayList<>();

        try {

            for (ResponsePhoto responsePhoto : getRecentResponse.photos.photos) {

                Photo photo = new Photo();

                photo.setId(Long.parseLong(responsePhoto.id));
                photo.setOwner(responsePhoto.owner);
                photo.setTitle(responsePhoto.title);
                photo.setFarm(responsePhoto.farm);
                photo.setServer(responsePhoto.server);
                photo.setSecret(responsePhoto.secret);

                photos.add(photo);
            }

        } catch (Exception exception) {
            Log.e("getPhotoObjects", exception.getMessage());
        }

        if (photos.size() > 0) {
            photos.remove(photos.size() - 1);
        }

        return photos;
    }

    public static List<Item> getPhotosListItems(App app, List<Photo> photos, boolean addLoaderItems) {

        List<Item> items = new ArrayList<>();

        for (int i = 0; i < photos.size(); i++) {

            Photo photo = photos.get(i);

            PhotoListItem item = new PhotoListItem();

            try {

                item.setType(AppViewFactory.PHOTOS_LIST_ITEM_VIEW);
                item.setId(photo.getId());
                item.setPhotoUrl(TextUtil.getImageThumbString(app, photo.getFarm(), photo.getServer(), photo.getId(), photo.getSecret()));
                item.setPhotoTitle(photo.getTitle());

            } catch (Exception ex) {
                Log.e(MapperUtil.class.getName(), "getPhotosListItems -> " + ex.getMessage());
            }

            items.add(item);
        }

        if (addLoaderItems && items.size() > 0) {
            addLoaderItems(items);
        }

        return items;
    }

    private static void addLoaderItems(List<Item> items) {

        PhotoListItem emptyItem = new PhotoListItem();
        emptyItem.setType(AppViewFactory.PHOTOS_LIST_ITEM_EMPTY_VIEW);

        PhotoListItem loaderItem = new PhotoListItem();
        loaderItem.setType(AppViewFactory.PHOTOS_LIST_ITEM_LOADER_VIEW);

        items.add(emptyItem);
        items.add(loaderItem);
        items.add(emptyItem);
    }
}
