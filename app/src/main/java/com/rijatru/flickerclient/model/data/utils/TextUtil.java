package com.rijatru.flickerclient.model.data.utils;

import com.rijatru.flickerclient.App;
import com.rijatru.flickerclient.R;

import java.text.Normalizer;

public final class TextUtil {

    public TextUtil() {
    }

    public static String getString(App app, int stringResource) {
        return app.getApplicationContext().getString(stringResource);
    }

    public static String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return s;
    }

    public static String getImageThumbString(App app, int farm, String server, long id, String secret) {
        return app.getApplicationContext().getString(R.string.flicker_photos_url, String.valueOf(farm), server, String.valueOf(id), secret);
    }

    public static String getImageLargeString(App app, int farm, String server, long id, String secret) {
        return app.getApplicationContext().getString(R.string.flicker_photos_url_large, String.valueOf(farm), server, String.valueOf(id), secret);
    }

    public static String getString(App app, int stringResource, Object... parameters) {
        return app.getApplicationContext().getString(stringResource, parameters);
    }
}
