package com.rijatru.flickerclient.view;

import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.rijatru.flickerclient.FlickerApp;
import com.rijatru.flickerclient.R;
import com.rijatru.flickerclient.dependencyInjection.component.DaggerViewsComponent;
import com.rijatru.flickerclient.dependencyInjection.component.ViewsComponent;
import com.rijatru.flickerclient.model.data.ConnectivityReceiver;
import com.rijatru.flickerclient.model.data.interfaces.AppConnectivityManager;
import com.rijatru.flickerclient.model.data.interfaces.AppConnectivityReceiver;
import com.rijatru.flickerclient.view.interfaces.FragmentListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public abstract class BaseActivity extends AppCompatActivity implements FragmentListener, ConnectivityReceiver.ConnectivityReceiverListener {

    private ViewsComponent component;
    private List<BaseFragment> pendingForClose = new ArrayList<>();
    private List<BaseFragment> pendingForOpen = new ArrayList<>();
    private FragmentManager fragmentManager;
    private boolean pause = false;
    private Snackbar snackBar;

    @Inject
    AppConnectivityReceiver connectivityReceiver;
    @Inject
    AppConnectivityManager connectivityManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fragmentManager = getSupportFragmentManager();

        component = DaggerViewsComponent.builder()
                .appComponent(FlickerApp.getApp().getAppComponent())
                .build();

        component.inject(this);

        connectivityReceiver.setListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        pause = true;
        unRegisterConnectivityReceiver();
    }

    @Override
    protected void onResume() {
        super.onResume();
        pause = false;
        checkPendingView();
        registerConnectivityReceiver();
    }

    protected ViewsComponent getComponent() {
        return component;
    }

    void addFragment(BaseFragment BaseFragment) {

        try {

            if (isPause()) {

                pendingForOpen.add(BaseFragment);

            } else {

                FragmentTransaction transaction = fragmentManager.beginTransaction();

                if (fragmentManager.getBackStackEntryCount() >= 0) {
                    transaction.setCustomAnimations(BaseFragment.getEnter(), BaseFragment.getExit(), BaseFragment.getPopEnter(), BaseFragment.getPopExit());
                }

                transaction.add(BaseFragment.getContainer(), BaseFragment, BaseFragment.getName());
                transaction.addToBackStack(BaseFragment.getName());
                transaction.commit();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void replaceFragment(BaseFragment newFragment) {

        if (isPause()) {

            pendingForOpen.add(newFragment);

        } else {

            FragmentTransaction transaction = fragmentManager.beginTransaction();
            BaseFragment oldFragment = null;

            if (fragmentManager.getBackStackEntryCount() > 0) {
                oldFragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(fragmentManager.getBackStackEntryAt(0).getName());
            }

            if (!newFragment.getName().equals(oldFragment != null ? oldFragment.getName() : "")) {

                if (oldFragment != null && oldFragment.isAddOnStack()) {
                    fragmentManager.popBackStack(oldFragment.getName(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }

                if (newFragment.isAddOnStack()) {
                    transaction.addToBackStack(newFragment.getName());
                }

                if (newFragment.isAnimate()) {
                    transaction.setCustomAnimations(newFragment.getEnter(), newFragment.getExit(), newFragment.getPopEnter(), newFragment.getPopExit());
                }

                transaction.replace(newFragment.getContainer(), newFragment, newFragment.getName());
                transaction.commit();

            } else {

                if (oldFragment != null) {
                    oldFragment.onResetState();
                }
            }
        }
    }

    private boolean isPause() {
        return pause;
    }

    private void checkPendingView() {

        for (BaseFragment BaseFragment : pendingForOpen) {
            addFragment(BaseFragment.setEnter(0).setPopEnter(0));
        }

        pendingForOpen.clear();
        pendingForOpen = new ArrayList<>();

        for (BaseFragment BaseFragment : pendingForClose) {
            onClose(BaseFragment);
        }

        pendingForClose.clear();
        pendingForClose = new ArrayList<>();
    }

    @Override
    public void onClose(BaseFragment view) {

        if (!isPause()) {

            if (fragmentManager.getBackStackEntryCount() < 1) {
                finish();
            }

            fragmentManager.popBackStack(view.getName(), FragmentManager.POP_BACK_STACK_INCLUSIVE);

        } else {

            pendingForClose.add(view);
        }
    }

    private void registerConnectivityReceiver() {
        registerReceiver(connectivityReceiver.getReceiver(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private void unRegisterConnectivityReceiver() {
        unregisterReceiver(connectivityReceiver.getReceiver());
    }

    @Override
    public void onConnectivityChange(boolean isConnected) {

        connectivityManager.notifyChange(isConnected);

        if (!isConnected) {

            snackBar = Snackbar.make(findViewById(R.id.container), getString(R.string.no_connectivity_title), Snackbar.LENGTH_INDEFINITE);
            snackBar.setActionTextColor(Color.WHITE);

            View sbView = snackBar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);

            sbView.setBackgroundColor(ContextCompat.getColor(getBaseContext(), R.color.colorAccent));

            snackBar.show();

        } else if (snackBar != null) {

            snackBar.dismiss();
        }
    }
}
