package com.rijatru.flickerclient.view;

import android.app.Service;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.rijatru.flickerclient.FlickerApp;
import com.rijatru.flickerclient.R;
import com.rijatru.flickerclient.dependencyInjection.component.DaggerViewsComponent;
import com.rijatru.flickerclient.dependencyInjection.component.ViewsComponent;

public abstract class BaseFragment extends Fragment {

    private ViewsComponent component;

    private int enter = R.anim.fade_in;
    private int exit = R.anim.fade_out;
    private int popEnter = R.anim.fade_in;
    private int popExit = R.anim.fade_out;
    private boolean addOnStack = false;
    private boolean animate = false;
    private int container = R.id.container;
    private boolean canBack = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        component = DaggerViewsComponent.builder()
                .appComponent(FlickerApp.getApp().getAppComponent())
                .build();
    }

    protected ViewsComponent getComponent() {
        return component;
    }

    public abstract String getName();

    public abstract boolean canBack();

    public void backPressed() {
    }

    public int getEnter() {
        return enter;
    }

    public BaseFragment setEnter(int enter) {
        this.enter = enter;
        return this;
    }

    public int getExit() {
        return exit;
    }

    public BaseFragment setExit(int exit) {
        this.exit = exit;
        return this;
    }

    public int getPopEnter() {
        return popEnter;
    }

    public BaseFragment setPopEnter(int popEnter) {
        this.popEnter = popEnter;
        return this;
    }

    public int getPopExit() {
        return popExit;
    }

    public BaseFragment setPopExit(int popExit) {
        this.popExit = popExit;
        return this;
    }

    public boolean isAddOnStack() {
        return addOnStack;
    }

    public boolean isAnimate() {
        return animate;
    }

    public int getContainer() {
        return container;
    }

    public BaseFragment addOnStack() {
        this.addOnStack = true;
        return this;
    }

    public BaseFragment animate() {
        this.animate = true;
        return this;
    }

    public BaseFragment id(int container) {
        this.container = container;
        return this;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {

        if (!isAnimate()) {

            onEnterAnimationEnd();

            return super.onCreateAnimation(transit, enter, nextAnim);
        }

        Animation anim;

        if (enter) {

            anim = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);

        } else {

            anim = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
        }

        anim.setAnimationListener(new Animation.AnimationListener() {

            public void onAnimationEnd(Animation animation) {
                onEnterAnimationEnd();
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });

        return anim;
    }

    protected void onEnterAnimationEnd() {

    }

    public boolean isCanBack() {
        return canBack;
    }

    public void setCanBack(boolean canBack) {
        this.canBack = canBack;
    }

    public void onResetState() {

    }

    protected void showKeyboard(View view) {

        if (isAdded()) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, 0);
        }
    }

    protected void hideKeyboard(View view) {

        if (isAdded()) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected void showToast(String text) {

        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(getContext(), text, duration);
        toast.show();
    }
}
