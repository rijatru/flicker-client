package com.rijatru.flickerclient.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.rijatru.flickerclient.FlickerApp;
import com.rijatru.flickerclient.dependencyInjection.component.DaggerViewsComponent;
import com.rijatru.flickerclient.dependencyInjection.component.ViewsComponent;
import com.rijatru.flickerclient.model.interfaces.Item;
import com.rijatru.flickerclient.view.interfaces.ItemView;

public abstract class BaseListView extends FrameLayout implements ItemView {

    private ViewsComponent component;

    public BaseListView(Context context) {
        super(context);
    }

    public BaseListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected void init(Context context) {

        component = DaggerViewsComponent.builder().appComponent(FlickerApp.getApp().getAppComponent()).build();
    }

    protected ViewsComponent getComponent() {
        return component;
    }

    @Override
    public abstract void bind(Item item, int position);
}
