package com.rijatru.flickerclient.view;

import android.os.Bundle;

import com.rijatru.flickerclient.R;
import com.rijatru.flickerclient.view.factories.AppViewFactory;
import com.rijatru.flickerclient.view.factories.interfaces.ViewFactory;

import javax.inject.Inject;

public class PhotoDetailActivity extends BaseActivity {

    @Inject
    ViewFactory viewsFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_base);
        getComponent().inject(this);

        if (savedInstanceState == null) {
            replaceFragment(viewsFactory.getAppView(AppViewFactory.PHOTOS_DETAIL_VIEW, getIntent().getExtras()));
        }
    }
}
