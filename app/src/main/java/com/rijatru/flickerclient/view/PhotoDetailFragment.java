package com.rijatru.flickerclient.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rijatru.flickerclient.App;
import com.rijatru.flickerclient.R;
import com.rijatru.flickerclient.databinding.FragmentPhotoDetailBinding;
import com.rijatru.flickerclient.databinding.FragmentPhotosBinding;
import com.rijatru.flickerclient.model.data.RxBus;
import com.rijatru.flickerclient.model.data.interfaces.PhotosManager;
import com.rijatru.flickerclient.view.interfaces.FragmentListener;
import com.rijatru.flickerclient.viewModel.factories.AppViewModelFactory;
import com.rijatru.flickerclient.viewModel.factories.interfaces.ViewModelFactory;
import com.rijatru.flickerclient.viewModel.interfaces.PhotoDetailViewMvvm;

import javax.inject.Inject;

public class PhotoDetailFragment extends BaseFragment implements PhotoDetailViewMvvm.View {

    private static final String BUNDLE_PHOTOS_RECYCLER = "recyclerViewPhotos";

    private FragmentPhotoDetailBinding binding;
    private Bundle savedInstanceState;
    private FragmentListener activity;
    private PhotoDetailViewMvvm.ViewModel viewModel;
    private long photoId;

    @Inject
    App app;
    @Inject
    PhotosManager photosManager;
    @Inject
    ViewModelFactory viewModelFactory;

    public static PhotoDetailFragment getInstance(Bundle data) {
        PhotoDetailFragment fragment = new PhotoDetailFragment();
        fragment.setArguments(data);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        getComponent().inject(this);

        Bundle bundle = getArguments();
        photoId = bundle.getLong(RxBus.PHOTO);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_photo_detail, container, false);

        viewModel = (PhotoDetailViewMvvm.ViewModel) viewModelFactory.getViewModel(this, AppViewModelFactory.PHOTO_DETAIL_VIEW_MODEL);
        viewModel.subscribe();
        viewModel.setPhotoId(photoId);
        binding.setViewModel(viewModel);

        return binding.getRoot();
    }

    @Override
    public void onPause() {
        super.onPause();
        viewModel.unSubscribe();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        initListener(context);
    }

    private void initListener(Context context) {
        try {
            activity = (FragmentListener) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getName() {
        return getClass().getName();
    }

    @Override
    public boolean canBack() {
        return false;
    }
}
