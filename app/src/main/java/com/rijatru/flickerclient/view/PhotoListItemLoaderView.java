package com.rijatru.flickerclient.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import com.rijatru.flickerclient.R;
import com.rijatru.flickerclient.model.interfaces.Item;
import com.rijatru.flickerclient.view.interfaces.ItemView;
import com.rijatru.flickerclient.viewModel.interfaces.PhotoItemViewMvvm;

public class PhotoListItemLoaderView extends BaseListView implements ItemView, PhotoItemViewMvvm.View {

    public PhotoListItemLoaderView(Context context) {
        super(context);
        init(context);
    }

    public PhotoListItemLoaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PhotoListItemLoaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @Override
    protected void init(Context context) {
        super.init(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        DataBindingUtil.inflate(inflater, R.layout.photo_item_loader_view, this, true);
    }

    @Override
    public void bind(Item item, int position) {

    }
}
