package com.rijatru.flickerclient.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Toast;

import com.rijatru.flickerclient.R;
import com.rijatru.flickerclient.databinding.PhotoItemViewBinding;
import com.rijatru.flickerclient.model.interfaces.Item;
import com.rijatru.flickerclient.view.interfaces.ItemView;
import com.rijatru.flickerclient.viewModel.factories.AppViewModelFactory;
import com.rijatru.flickerclient.viewModel.factories.interfaces.ViewModelFactory;
import com.rijatru.flickerclient.viewModel.interfaces.PhotoItemViewMvvm;

import javax.inject.Inject;

public class PhotoListItemView extends BaseListView implements ItemView, PhotoItemViewMvvm.View {

    private PhotoItemViewBinding binding;
    private PhotoItemViewMvvm.ViewModel viewModel;

    @Inject
    ViewModelFactory viewModelFactory;

    public PhotoListItemView(Context context) {
        super(context);
        init(context);
    }

    public PhotoListItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PhotoListItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @Override
    protected void init(Context context) {
        super.init(context);

        getComponent().inject(this);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding = DataBindingUtil.inflate(inflater, R.layout.photo_item_view, this, true);

        binding.itemImage.setOnLongClickListener(view -> {

            String text = viewModel.getTitleString();
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

            return true;
        });
    }

    @Override
    public void bind(Item item, int position) {

        viewModel = (PhotoItemViewMvvm.ViewModel) viewModelFactory.getItemViewModel(this, AppViewModelFactory.PHOTO_LIST_ITEM_VIEW_MODEL, item);
        binding.setViewModel(viewModel);
    }
}
