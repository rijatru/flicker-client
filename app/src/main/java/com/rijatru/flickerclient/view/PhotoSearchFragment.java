package com.rijatru.flickerclient.view;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;

import com.rijatru.flickerclient.App;
import com.rijatru.flickerclient.R;
import com.rijatru.flickerclient.databinding.FragmentSearchBinding;
import com.rijatru.flickerclient.model.data.interfaces.PhotosManager;
import com.rijatru.flickerclient.view.adapters.interfaces.ListAdapter;
import com.rijatru.flickerclient.view.interfaces.FragmentListener;
import com.rijatru.flickerclient.view.interfaces.ToolBarListener;
import com.rijatru.flickerclient.view.utils.DecoratorsUtil;
import com.rijatru.flickerclient.view.utils.LayoutManagerUtil;
import com.rijatru.flickerclient.viewModel.factories.AppViewModelFactory;
import com.rijatru.flickerclient.viewModel.factories.interfaces.ViewModelFactory;
import com.rijatru.flickerclient.viewModel.interfaces.PhotosSearchViewMvvm;

import javax.inject.Inject;

public class PhotoSearchFragment extends BaseFragment implements PhotosSearchViewMvvm.View {

    private static final String BUNDLE_SEARCH_RECYCLER = "recyclerViewPhotos";
    private final String SEARCH_TERM = "searchTerm";

    private Bundle savedInstanceState;
    private FragmentSearchBinding binding;
    private FragmentListener activity;
    private PhotosSearchViewMvvm.ViewModel viewModel;
    private GridLayoutManager layoutManager;
    private RecyclerView.ItemDecoration itemDecoration;

    @Inject
    App app;
    @Inject
    PhotosManager photosManager;
    @Inject
    ViewModelFactory viewModelFactory;

    public static PhotoSearchFragment getInstance(Bundle data) {
        PhotoSearchFragment fragment = new PhotoSearchFragment();
        fragment.setArguments(data);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        getComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false);

        viewModel = (PhotosSearchViewMvvm.ViewModel) viewModelFactory.getViewModel(this, AppViewModelFactory.PHOTO_SEARCH_VIEW_MODEL);
        viewModel.subscribe();
        binding.setViewModel(viewModel);

        if (savedInstanceState != null) {
            viewModel.setSearchTerm(savedInstanceState.getString(SEARCH_TERM));
        }

        initToolbar();
        initSearchField();

        return binding.getRoot();
    }

    @Override
    public void onPause() {
        super.onPause();
        viewModel.unSubscribe();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        initListener(context);
    }

    private void initSearchField() {

        binding.searchField.setOnEditorActionListener((v, actionId, event) -> {

            if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                search();

                return true;
            }

            return false;
        });
    }

    private void initToolbar() {

        binding.toolBar.setListener(new ToolBarListener() {
            @Override
            public void onLeftIconClick() {
                photosManager.resetSearchResults();
                photosManager.clearSearchResults();
                activity.onClose(PhotoSearchFragment.this);
            }

            @Override
            public void onRightEndIconClick() {
                search();
            }
        });
    }

    private void initListener(Context context) {
        try {
            activity = (FragmentListener) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(SEARCH_TERM, viewModel.getSearchTerm());

        if (binding.photosList.getLayoutManager() != null) {
            outState.putParcelable(BUNDLE_SEARCH_RECYCLER, binding.photosList.getLayoutManager().onSaveInstanceState());
        }
    }

    @Override
    public void onListOfPhotosReady(ListAdapter adapter) {

        if (isAdded() && layoutManager == null) {

            layoutManager = LayoutManagerUtil.getPhotosListLayoutManager(app, savedInstanceState, BUNDLE_SEARCH_RECYCLER);

            if (itemDecoration == null) {

                itemDecoration = DecoratorsUtil.getPhotosListItemDecorator(app);
                binding.photosList.addItemDecoration(itemDecoration);
            }

            binding.photosList.setLayoutManager(layoutManager);
            binding.photosList.setItemAnimator(new DefaultItemAnimator());
            binding.photosList.setAdapter((RecyclerView.Adapter) adapter);

            binding.photosList.clearOnScrollListeners();
            binding.photosList.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {

                @Override
                public void onLoadMore(int currentPage) {
                    viewModel.searchMorePhotos();
                }
            });
        }
    }

    @Override
    public String getName() {
        return getClass().getName();
    }

    @Override
    public boolean canBack() {
        return false;
    }

    private void search() {

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);

        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

        viewModel.searchPhotos(binding.searchField.getText().toString());
    }
}
