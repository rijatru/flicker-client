package com.rijatru.flickerclient.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rijatru.flickerclient.App;
import com.rijatru.flickerclient.R;
import com.rijatru.flickerclient.databinding.FragmentPhotosBinding;
import com.rijatru.flickerclient.model.data.LogManager;
import com.rijatru.flickerclient.view.adapters.interfaces.ListAdapter;
import com.rijatru.flickerclient.view.interfaces.FragmentListener;
import com.rijatru.flickerclient.view.interfaces.PhotosFragmentListener;
import com.rijatru.flickerclient.view.interfaces.ToolBarListener;
import com.rijatru.flickerclient.view.utils.DecoratorsUtil;
import com.rijatru.flickerclient.view.utils.LayoutManagerUtil;
import com.rijatru.flickerclient.viewModel.factories.AppViewModelFactory;
import com.rijatru.flickerclient.viewModel.factories.interfaces.ViewModelFactory;
import com.rijatru.flickerclient.viewModel.interfaces.PhotosViewMvvm;

import javax.inject.Inject;

public class PhotosFragment extends BaseFragment implements PhotosViewMvvm.View {

    private static final String BUNDLE_PHOTOS_RECYCLER = "recyclerViewPhotos";

    private FragmentPhotosBinding binding;
    private Bundle savedInstanceState;
    private FragmentListener activity;
    private PhotosViewMvvm.ViewModel viewModel;
    private GridLayoutManager layoutManager;
    private RecyclerView.ItemDecoration itemDecoration;

    @Inject
    App app;
    @Inject
    ViewModelFactory viewModelFactory;
    @Inject
    LogManager logManager;

    public static PhotosFragment getInstance(Bundle data) {
        PhotosFragment fragment = new PhotosFragment();
        fragment.setArguments(data);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        getComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_photos, container, false);

        viewModel = (PhotosViewMvvm.ViewModel) viewModelFactory.getViewModel(this, AppViewModelFactory.PHOTOS_LIST_CONTAINER_VIEW_MODEL);
        viewModel.subscribe();

        binding.swipeRefresh.setColorSchemeResources(R.color.colorAccent);
        binding.swipeRefresh.setOnRefreshListener(() -> viewModel.getNewPhotos());

        initToolbar();

        return binding.getRoot();
    }

    @Override
    public void onPause() {
        super.onPause();
        viewModel.unSubscribe();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        initListener(context);
    }

    private void initToolbar() {

        binding.toolBar.setListener(new ToolBarListener() {
            @Override
            public void onLeftIconClick() {
                activity.onClose(PhotosFragment.this);
            }

            @Override
            public void onRightEndIconClick() {
                ((PhotosFragmentListener) activity).openSearch();
            }
        });
    }

    private void initListener(Context context) {
        try {
            activity = (FragmentListener) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (binding.photosList.getLayoutManager() != null) {
            outState.putParcelable(BUNDLE_PHOTOS_RECYCLER, binding.photosList.getLayoutManager().onSaveInstanceState());
        }
    }

    @Override
    public String getName() {
        return getClass().getName();
    }

    @Override
    public boolean canBack() {
        return false;
    }

    @Override
    public void onListOfPhotosReady(ListAdapter adapter) {

        if (isAdded()) {

            binding.swipeRefresh.setRefreshing(false);

            layoutManager = LayoutManagerUtil.getPhotosListLayoutManager(app, savedInstanceState, BUNDLE_PHOTOS_RECYCLER);

            if (itemDecoration == null) {

                itemDecoration = DecoratorsUtil.getPhotosListItemDecorator(app);
                binding.photosList.addItemDecoration(itemDecoration);
            }

            binding.photosList.setLayoutManager(layoutManager);
            binding.photosList.setItemAnimator(new DefaultItemAnimator());
            binding.photosList.setAdapter((RecyclerView.Adapter) adapter);
            binding.photosList.clearOnScrollListeners();
            binding.photosList.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {

                @Override
                public void onLoadMore(int currentPage) {
                    viewModel.loadMorePhotos();
                }
            });
        }
    }

    @Override
    public void onNewPhotos() {
        binding.swipeRefresh.setRefreshing(false);
        layoutManager.scrollToPositionWithOffset(0, 0);
    }

    @Override
    public void onPhotosLoaded() {
        binding.swipeRefresh.setRefreshing(false);
    }
}
