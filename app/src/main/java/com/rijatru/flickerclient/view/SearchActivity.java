package com.rijatru.flickerclient.view;

import android.content.Intent;
import android.os.Bundle;

import com.rijatru.flickerclient.R;
import com.rijatru.flickerclient.model.data.RxBus;
import com.rijatru.flickerclient.model.data.interfaces.Bus;
import com.rijatru.flickerclient.view.factories.AppViewFactory;
import com.rijatru.flickerclient.view.factories.interfaces.ViewFactory;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class SearchActivity extends BaseActivity {

    private CompositeDisposable disposables = new CompositeDisposable();

    @Inject
    Bus bus;
    @Inject
    ViewFactory viewsFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_base);
        getComponent().inject(this);

        if (savedInstanceState == null) {
            replaceFragment(viewsFactory.getAppView(AppViewFactory.PHOTOS_SEARCH_VIEW, new Bundle()));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        subscribeToBus();
    }

    @Override
    protected void onPause() {
        super.onPause();
        disposables.clear();
    }

    private void subscribeToBus() {

        disposables.add(bus.getObservable()
                .doOnNext(bundle -> {

                    switch (bundle.getInt(RxBus.CODE)) {

                        case RxBus.OPEN_PHOTO:
                            openPhoto(bundle);
                            break;
                    }

                }).subscribe());
    }

    private void openPhoto(Bundle bundle) {

        Intent intent = new Intent(this, PhotoDetailActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
