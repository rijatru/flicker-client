package com.rijatru.flickerclient.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.rijatru.flickerclient.R;
import com.rijatru.flickerclient.databinding.ToolBarViewBinding;
import com.rijatru.flickerclient.view.interfaces.ToolBarListener;
import com.rijatru.flickerclient.view.utils.Animations;
import com.rijatru.flickerclient.viewModel.ToolbarViewModel;
import com.rijatru.flickerclient.viewModel.interfaces.ToolbarMvvm;

public class ToolbarView extends FrameLayout implements ToolbarMvvm.View {

    private String title;
    private Drawable iconLeft;
    private Drawable iconRightMid;
    private Drawable iconRightEnd;
    private int backgroundColor;
    private int textColor;

    private ToolBarViewBinding binding;
    private ToolBarListener listener;

    public ToolbarView(Context context) {
        super(context);

        init(context);
    }

    public ToolbarView(Context context, AttributeSet attrs) {
        super(context, attrs);

        initAttrs(context, attrs);
        init(context);
    }

    public ToolbarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initAttrs(context, attrs);
        init(context);
    }

    private void initAttrs(Context context, AttributeSet attrs) {

        if (!isInEditMode()) {

            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.toolbar);

            if (typedArray != null) {

                final int arraySize = typedArray.getIndexCount();

                for (int i = 0; i < arraySize; ++i) {

                    int attr = typedArray.getIndex(i);

                    switch (attr) {
                        case R.styleable.toolbar_title:
                            title = typedArray.getString(attr);
                            break;
                        case R.styleable.toolbar_left_icon:
                            iconLeft = typedArray.getDrawable(attr);
                            break;
                        case R.styleable.toolbar_right_mid_icon:
                            iconRightMid = typedArray.getDrawable(attr);
                            break;
                        case R.styleable.toolbar_right_end_icon:
                            iconRightEnd = typedArray.getDrawable(attr);
                            break;
                        case R.styleable.toolbar_background_color:
                            backgroundColor = typedArray.getColor(attr, -1);
                            break;
                        case R.styleable.toolbar_text_color:
                            textColor = typedArray.getColor(attr, -1);
                            break;
                    }
                }

                typedArray.recycle();
            }
        }
    }

    private void init(Context context) {

        if (!isInEditMode()) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            binding = DataBindingUtil.inflate(inflater, R.layout.tool_bar_view, this, true);

            ToolbarMvvm.ViewModel viewModel = new ToolbarViewModel(this);
            binding.setViewModel(viewModel);

            buildViews();
        }
    }

    private void buildViews() {

        setIconLeft();
        setToolbarTitle();
        setIconRightEnd();
        setBackgroundColor(backgroundColor);
        setTextColor(textColor);
    }

    private void setIconLeft() {

        if (iconLeft != null) {
            binding.imageLeft.setImageDrawable(iconLeft);
        } else {
            binding.imageLeft.setVisibility(GONE);
        }
    }

    public void setTitle(String title) {

        this.title = title;

        setToolbarTitle();
    }

    private void setToolbarTitle() {

        if (!TextUtils.isEmpty(title)) {
            binding.title.setText(title);
        }
    }

    private void setIconRightEnd() {

        if (iconRightEnd != null) {
            binding.imageRightEnd.setImageDrawable(iconRightEnd);
        }
    }

    public void setIconRightEnd(Drawable drawable) {

        if (drawable != null && iconRightEnd != null) {
            binding.imageRightEnd.setImageDrawable(drawable);
        }
    }

    @Override
    public void setListener(ToolBarListener listener) {
        this.listener = listener;
    }

    @Override
    public void onLeftIconClick() {
        if (listener != null) {
            listener.onLeftIconClick();
        }
    }

    @Override
    public void onRightEndIconClick() {
        if (listener != null) {
            listener.onRightEndIconClick();
        }
    }

    public void setTextColor(int textColor) {
        if (textColor != 0) {
            binding.title.setTextColor(textColor);
        }
    }

    public float[] getRightEndIconLocation() {

        float[] ratios = Animations.getDisplayRatios(getContext(), binding.imageRightEnd);

        ratios[1] = 0.5f;

        return ratios;
    }
}
