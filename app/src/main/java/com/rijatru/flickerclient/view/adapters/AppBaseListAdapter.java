package com.rijatru.flickerclient.view.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.rijatru.flickerclient.model.data.LogManager;
import com.rijatru.flickerclient.model.interfaces.Item;
import com.rijatru.flickerclient.view.ListItemViewHolder;
import com.rijatru.flickerclient.view.adapters.interfaces.ListAdapter;
import com.rijatru.flickerclient.view.factories.interfaces.ViewFactory;
import com.rijatru.flickerclient.view.interfaces.ItemView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class AppBaseListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ListAdapter {

    private final List<ItemView> viewsList;
    private List<Item> items;

    private ViewFactory viewFactory;
    private LogManager logManager;

    @Inject
    public AppBaseListAdapter(ViewFactory viewFactory, LogManager logManager) {

        this.viewFactory = viewFactory;
        this.logManager = logManager;

        viewsList = new ArrayList<>();

        notifyItemsSet();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = (View) this.viewFactory.getItemView(viewType);
        viewsList.add((ItemView) view);
        return new ListItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ItemView itemView = (ItemView) holder.itemView;
        itemView.bind(getItem(position), position);
    }

    @Override
    public void setItems(List<Item> items) {

        try {

            this.items = new ArrayList<>();

            if (items != null) {
                this.items.addAll(items);
            } else {
                this.items = new ArrayList<>();
            }

            this.notifyItemRangeChanged(0, this.items.size());

        } catch (Exception ex) {
            logManager.log(getClass().getName(), ex.getMessage());
        }
    }

    @Override
    public void addNewItems(List<Item> items) {

        for (Item item : items) {
            addItem(item);
        }
    }

    @Override
    public void removeLastItems(int numberOfItemsToRemove) {

        List<Item> itemsToRemove = new ArrayList<>();

        if (items != null && items.size() > numberOfItemsToRemove) {

            for (int i = items.size() - 1; i >= items.size() - numberOfItemsToRemove; i--) {
                itemsToRemove.add(items.get(i));
            }

            for (Item item : itemsToRemove) {
                removeItem(item);
            }
        }
    }

    @Override
    public void addNewItemsToBeginning(List<Item> newItems) {

        if (items == null) {
            items = new ArrayList<>();
        }

        for (Item item : newItems) {
            items.add(0, item);
        }

        notifyItemRangeInserted(0, newItems.size() - 1);
    }

    public void removeItem(Item item) {

        int index = this.items.indexOf(item);

        if (index != -1) {
            this.items.remove(index);
            this.notifyItemRemoved(index);
        }
    }

    public void addItem(Item item) {

        if (items == null) {
            items = new ArrayList<>();
        }

        items.add(item);
        notifyItemInserted(items.size() - 1);
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public Item getItem(int position) {
        return items.get(position);
    }

    private void notifyItemsSet() {
        notifyDataSetChanged();
    }
}
