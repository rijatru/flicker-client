package com.rijatru.flickerclient.view.adapters.interfaces;

import com.rijatru.flickerclient.model.interfaces.Item;

import java.util.List;

public interface ListAdapter {

    void setItems(List<Item> listItems);

    void addNewItems(List<Item> items);

    void removeLastItems(int numberOfItemsToRemove);

    void addNewItemsToBeginning(List<Item> newPhotos);

    int getItemCount();
}
