package com.rijatru.flickerclient.view.decorators;

import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

public class PhotoListItemDecorator extends RecyclerView.ItemDecoration {

    private final boolean isPortrait;
    private final int space;

    public PhotoListItemDecorator(boolean isPortrait, int space) {
        this.isPortrait = isPortrait;
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        int position = parent.getChildAdapterPosition(view);

        GridLayoutManager.LayoutParams lp = (GridLayoutManager.LayoutParams) view.getLayoutParams();
        int spanIndex = lp.getSpanIndex();

        if (isPortrait) {

            if (isTopRowInPortrait(position)) {
                outRect.top = space * 2;
            } else {
                outRect.top = space;
            }

            if (spanIndex == 0) {
                outRect.left = space * 2;
                outRect.right = space;
            } else if (spanIndex == 1) {
                outRect.left = space;
                outRect.right = space;
            } else {
                outRect.left = space;
                outRect.right = space * 2;
            }

            if (isBottomRowInPortrait(state, position)) {
                outRect.bottom = space * 2;
            } else {
                outRect.bottom = space;
            }

        } else {

            if (isTopRowInLandscape(position)) {
                outRect.top = space * 2;
            } else {
                outRect.top = space;
            }

            if (spanIndex == 0) {
                outRect.left = space * 2;
                outRect.right = space;
            } else if (spanIndex == 1 || spanIndex == 2) {
                outRect.left = space;
                outRect.right = space;
            } else {
                outRect.left = space;
                outRect.right = space * 2;
            }

            if (isBottomRowInLandscape(state, position)) {
                outRect.bottom = space * 2;
            } else {
                outRect.bottom = space;
            }
        }
    }

    private boolean isBottomRowInLandscape(RecyclerView.State state, int position) {

        return position == state.getItemCount() - 1 ||
                position == state.getItemCount() - 2 ||
                position == state.getItemCount() - 3 ||
                position == state.getItemCount() - 4;
    }

    private boolean isBottomRowInPortrait(RecyclerView.State state, int position) {

        return position == state.getItemCount() - 1 ||
                position == state.getItemCount() - 2 ||
                position == state.getItemCount() - 3;
    }

    private boolean isTopRowInPortrait(int position) {

        return position == 0 ||
                position == 1 ||
                position == 2;
    }

    private boolean isTopRowInLandscape(int position) {

        return position == 0 ||
                position == 1 ||
                position == 2 ||
                position == 3;
    }
}
