package com.rijatru.flickerclient.view.factories;

import android.os.Bundle;

import com.rijatru.flickerclient.App;
import com.rijatru.flickerclient.view.BaseFragment;
import com.rijatru.flickerclient.view.PhotoDetailFragment;
import com.rijatru.flickerclient.view.PhotoListItemEmptyView;
import com.rijatru.flickerclient.view.PhotoListItemLoaderView;
import com.rijatru.flickerclient.view.PhotoListItemView;
import com.rijatru.flickerclient.view.PhotoSearchFragment;
import com.rijatru.flickerclient.view.PhotosFragment;
import com.rijatru.flickerclient.view.factories.interfaces.ViewFactory;
import com.rijatru.flickerclient.view.interfaces.ItemView;

import javax.inject.Inject;

public class AppViewFactory implements ViewFactory {

    public static final int PHOTOS_LIST_CONTAINER_VIEW = 100;
    public static final int PHOTOS_LIST_ITEM_VIEW = 200;
    public static final int PHOTOS_LIST_ITEM_EMPTY_VIEW = 210;
    public static final int PHOTOS_LIST_ITEM_LOADER_VIEW = 220;
    public static final int PHOTOS_DETAIL_VIEW = 300;
    public static final int PHOTOS_SEARCH_VIEW = 400;

    private App app;

    @Inject
    public AppViewFactory(App app) {
        this.app = app;
    }

    @Override
    public ItemView getItemView(int type) {

        ItemView item;

        switch (type) {

            case PHOTOS_LIST_ITEM_VIEW:
                item = new PhotoListItemView(app.getApplicationContext());
                break;
            case PHOTOS_LIST_ITEM_EMPTY_VIEW:
                item = new PhotoListItemEmptyView(app.getApplicationContext());
                break;
            case PHOTOS_LIST_ITEM_LOADER_VIEW:
                item = new PhotoListItemLoaderView(app.getApplicationContext());
                break;
            default:
                item = null;
        }

        return item;
    }

    @Override
    public BaseFragment getAppView(int type, Bundle bundle) {

        BaseFragment view;

        switch (type) {

            case PHOTOS_LIST_CONTAINER_VIEW:
                view = PhotosFragment.getInstance(bundle);
                break;
            case PHOTOS_DETAIL_VIEW:
                view = PhotoDetailFragment.getInstance(bundle);
                break;
            case PHOTOS_SEARCH_VIEW:
                view = PhotoSearchFragment.getInstance(bundle);
                break;
            default:
                view = null;
        }

        return view;
    }
}
