package com.rijatru.flickerclient.view.factories.interfaces;

import android.os.Bundle;

import com.rijatru.flickerclient.view.BaseFragment;
import com.rijatru.flickerclient.view.interfaces.ItemView;

public interface ViewFactory {

    ItemView getItemView(int type);

    BaseFragment getAppView(int type, Bundle bundle);
}
