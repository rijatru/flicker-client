package com.rijatru.flickerclient.view.interfaces;

import com.rijatru.flickerclient.view.BaseFragment;

public interface FragmentListener {

    void onClose(BaseFragment view);
}
