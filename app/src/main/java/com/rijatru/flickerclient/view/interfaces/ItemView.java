package com.rijatru.flickerclient.view.interfaces;

import com.rijatru.flickerclient.model.interfaces.Item;

public interface ItemView {

    void bind(Item item, int position);
}
