package com.rijatru.flickerclient.view.interfaces;

public interface PhotosFragmentListener extends FragmentListener {

    void openSearch();
}
