package com.rijatru.flickerclient.view.interfaces;

public interface ToolBarListener {

    void onLeftIconClick();

    void onRightEndIconClick();
}
