package com.rijatru.flickerclient.view.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewAnimationUtils;

public class Animations {

    public static void enterReveal(View view, float[] locationOnScreen) {

        int width;
        int height;

        int dx = (int) (view.getMeasuredWidth() * locationOnScreen[0]);
        int dy = (int) (view.getMeasuredHeight() * locationOnScreen[1]);

        width = (dx <= view.getRight() - dx) ? view.getMeasuredWidth() - dx : dx;
        height = (dy <= view.getBottom() - dy) ? view.getMeasuredHeight() - dy : dy;

        int finalRadius = (int) Math.sqrt(width * width + height * height);

        Animator anim = ViewAnimationUtils.createCircularReveal(view, dx, dy, 0, finalRadius);

        view.setVisibility(View.VISIBLE);
        anim.setDuration(300);
        anim.start();
    }

    public static void exitReveal(final View view, float[] locationOnScreen) {
        exitReveal(view, locationOnScreen, null)   ;
    }

    public static void exitReveal(final View view, float[] locationOnScreen, AnimatorListenerAdapter listenerAdapter) {

        int dx = (int) (view.getMeasuredWidth() * locationOnScreen[0]);
        int dy = (int) (view.getMeasuredHeight() * locationOnScreen[1]);

        final Animator anim = ViewAnimationUtils.createCircularReveal(view, dx, dy, view.getWidth(), 0);

        anim.setDuration(300);
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.INVISIBLE);
                if (listenerAdapter != null) {
                    listenerAdapter.onAnimationEnd(anim);
                }
            }
        });

        anim.start();
    }

    public static float[] getDisplayRatios(Context context, View view) {
        int[] locations = new int[2];
        view.getLocationOnScreen(locations);
        return getDisplayRatios(context, locations, view.getWidth(), view.getHeight());
    }

    private static float[] getDisplayRatios(Context context, int[] locations, int viewWidth, int viewHeight) {

        float[] ratios = new float[2];

        int x = locations[0] + viewWidth / 2;
        int y = locations[1] + viewHeight / 2;

        DisplayMetrics displayMetrics = new DisplayMetrics();

        ((Activity) context).getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);

        ratios[0] = x / (float) displayMetrics.widthPixels;
        ratios[1] = y / (float) displayMetrics.heightPixels;

        return ratios;
    }
}
