package com.rijatru.flickerclient.view.utils;

import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

public class BindingAdapters {

    @BindingAdapter({"thumbnail"})
    public static void getPhoto(ImageView imageView, final String url) {

        if (!TextUtils.isEmpty(url)) {

            Picasso.with(imageView.getContext())
                    .load(url)
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .config(Bitmap.Config.RGB_565)
                    .into(imageView, new Callback() {

                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {

                            Picasso.with(imageView.getContext())
                                    .load(url)
                                    .config(Bitmap.Config.RGB_565)
                                    .into(imageView);
                        }
                    });
        }
    }
}
