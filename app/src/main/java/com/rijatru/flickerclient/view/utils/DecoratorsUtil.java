package com.rijatru.flickerclient.view.utils;

import android.support.v7.widget.RecyclerView;

import com.rijatru.flickerclient.App;
import com.rijatru.flickerclient.R;
import com.rijatru.flickerclient.view.decorators.PhotoListItemDecorator;

public class DecoratorsUtil {

    public static RecyclerView.ItemDecoration getPhotosListItemDecorator(App app) {
        return new PhotoListItemDecorator(app.getApplicationContext().getResources().getBoolean(R.bool.is_portrait), (int) app.getApplicationContext().getResources().getDimension(R.dimen.dimen_micro_plus));
    }
}
