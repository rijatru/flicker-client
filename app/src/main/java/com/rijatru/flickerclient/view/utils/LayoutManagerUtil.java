package com.rijatru.flickerclient.view.utils;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.GridLayoutManager;

import com.rijatru.flickerclient.App;
import com.rijatru.flickerclient.R;

public class LayoutManagerUtil {

    public static GridLayoutManager getPhotosListLayoutManager(App app, Bundle savedInstanceState, String bundleKey) {

        GridLayoutManager manager = new GridLayoutManager(app.getApplicationContext(), app.getApplicationContext().getResources().getInteger(R.integer.span_photos));

        if (savedInstanceState != null) {

            Parcelable savedRecyclerLayoutState = savedInstanceState.getParcelable(bundleKey);

            if (savedRecyclerLayoutState != null) {
                manager.onRestoreInstanceState(savedRecyclerLayoutState);
            }
        }

        return manager;
    }
}
