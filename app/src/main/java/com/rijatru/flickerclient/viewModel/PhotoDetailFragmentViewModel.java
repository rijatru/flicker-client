package com.rijatru.flickerclient.viewModel;

import android.databinding.ObservableField;
import android.os.Bundle;
import android.view.View;

import com.rijatru.flickerclient.App;
import com.rijatru.flickerclient.model.Photo;
import com.rijatru.flickerclient.model.PhotoListItem;
import com.rijatru.flickerclient.model.data.RxBus;
import com.rijatru.flickerclient.model.data.interfaces.Bus;
import com.rijatru.flickerclient.model.data.interfaces.PhotosManager;
import com.rijatru.flickerclient.model.data.utils.TextUtil;
import com.rijatru.flickerclient.model.interfaces.Item;
import com.rijatru.flickerclient.view.interfaces.AppView;
import com.rijatru.flickerclient.viewModel.interfaces.PhotoDetailViewMvvm;
import com.rijatru.flickerclient.viewModel.interfaces.PhotoItemViewMvvm;

public class PhotoDetailFragmentViewModel implements PhotoDetailViewMvvm.ViewModel {

    private final PhotosManager photosManager;
    private final App app;
    private final AppView view;

    private ObservableField<String> image;
    private ObservableField<String> title;
    private ObservableField<Long> id;

    public PhotoDetailFragmentViewModel(App app, AppView view, PhotosManager photosManager) {

        this.app = app;
        this.photosManager = photosManager;
        this.view = view;

        this.image = new ObservableField<>();
        this.title = new ObservableField<>();
        this.id = new ObservableField<>();
    }

    @Override
    public String getImage() {
        return image.get();
    }

    @Override
    public String getTitle() {
        return title.get();
    }

    @Override
    public String getId() {
        return String.valueOf(id.get());
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }

    @Override
    public void setPhotoId(long photoId) {

        Photo photo = photosManager.getPhotoById(photoId);

        this.image.set(TextUtil.getImageThumbString(app, photo.getFarm(), photo.getServer(), photo.getId(), photo.getSecret()));
        this.title.set(photo.getTitle());
        this.id.set(photo.getId());
    }
}
