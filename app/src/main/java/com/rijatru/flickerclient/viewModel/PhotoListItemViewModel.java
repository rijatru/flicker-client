package com.rijatru.flickerclient.viewModel;

import android.databinding.ObservableField;
import android.os.Bundle;
import android.view.View;

import com.rijatru.flickerclient.model.PhotoListItem;
import com.rijatru.flickerclient.model.data.RxBus;
import com.rijatru.flickerclient.model.data.interfaces.Bus;
import com.rijatru.flickerclient.model.interfaces.Item;
import com.rijatru.flickerclient.view.interfaces.AppView;
import com.rijatru.flickerclient.viewModel.interfaces.PhotoItemViewMvvm;

public class PhotoListItemViewModel implements PhotoItemViewMvvm.ViewModel {

    private final PhotoListItem item;
    private final Bus bus;
    private ObservableField<String> image;
    private ObservableField<String> title;
    private ObservableField<Long> id;

    public PhotoListItemViewModel(AppView view, Item item, Bus bus) {
        this.bus = bus;
        this.item = ((PhotoListItem) item);
        this.image = new ObservableField<>();
        this.title = new ObservableField<>();
        this.id = new ObservableField<>();
        this.image.set(this.item.getPhotoUrl());
        this.title.set(this.item.getPhotoTitle());
        this.id.set(this.item.getId());
    }

    @Override
    public String getImage() {
        return image.get();
    }

    @Override
    public String getTitleString() {
        return title.get();
    }

    @Override
    public void onClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt(RxBus.CODE, RxBus.OPEN_PHOTO);
        bundle.putLong(RxBus.PHOTO, id.get());
        bus.publish(bundle);
    }
}
