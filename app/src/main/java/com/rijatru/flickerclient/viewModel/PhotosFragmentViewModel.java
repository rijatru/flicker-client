package com.rijatru.flickerclient.viewModel;

import com.rijatru.flickerclient.App;
import com.rijatru.flickerclient.model.Photo;
import com.rijatru.flickerclient.model.data.interfaces.PhotosManager;
import com.rijatru.flickerclient.model.data.utils.MapperUtil;
import com.rijatru.flickerclient.model.interfaces.Item;
import com.rijatru.flickerclient.view.adapters.interfaces.ListAdapter;
import com.rijatru.flickerclient.view.interfaces.AppView;
import com.rijatru.flickerclient.viewModel.factories.interfaces.ListAdapterFactory;
import com.rijatru.flickerclient.viewModel.interfaces.PhotosViewMvvm;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class PhotosFragmentViewModel implements PhotosViewMvvm.ViewModel {

    private final App app;
    private final PhotosViewMvvm.View view;
    private final ListAdapterFactory listAdapterFactory;
    private final PhotosManager photosManager;

    private ListAdapter adapter;
    private CompositeDisposable disposables = new CompositeDisposable();

    public PhotosFragmentViewModel(App app, AppView view, ListAdapterFactory listAdapterFactory, PhotosManager photosManager) {

        this.view = (PhotosViewMvvm.View) view;

        this.app = app;
        this.listAdapterFactory = listAdapterFactory;
        this.photosManager = photosManager;
    }

    @Override
    public void subscribe() {

        disposables.add(photosManager.getPhotosObservable()
                .doOnNext(this::initAdapterAndSendToView)
                .subscribe());

        disposables.add(photosManager.getNewPhotosObservable()
                .doOnNext(newPhotos -> {
                    adapter.addNewItemsToBeginning(MapperUtil.getPhotosListItems(app, newPhotos, false));
                    view.onNewPhotos();
                }).subscribe());

        photosManager.getPhotos();
    }

    private void initAdapterAndSendToView(List<Photo> photos) {

        List<Item> photosListItems = MapperUtil.getPhotosListItems(app, photos, true);

        if (photosListItems.size() == 0) {
            photosManager.getNewPhotos();
        }

        if (adapter == null) {
            adapter = listAdapterFactory.getListAdapter();
            adapter.setItems(photosListItems);
            view.onListOfPhotosReady(adapter);
        } else {
            adapter.removeLastItems(3);
            adapter.addNewItems(photosListItems);
        }

        view.onPhotosLoaded();
    }

    @Override
    public void unSubscribe() {
        disposables.clear();
    }

    @Override
    public void loadMorePhotos() {
        photosManager.getMorePhotos();
    }

    @Override
    public void getNewPhotos() {
        photosManager.getNewPhotos();
    }
}
