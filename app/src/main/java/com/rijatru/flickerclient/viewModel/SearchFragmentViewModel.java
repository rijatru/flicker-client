package com.rijatru.flickerclient.viewModel;

import android.databinding.ObservableField;

import com.rijatru.flickerclient.App;
import com.rijatru.flickerclient.model.Photo;
import com.rijatru.flickerclient.model.data.interfaces.PhotosManager;
import com.rijatru.flickerclient.model.data.utils.MapperUtil;
import com.rijatru.flickerclient.model.interfaces.Item;
import com.rijatru.flickerclient.view.adapters.interfaces.ListAdapter;
import com.rijatru.flickerclient.view.interfaces.AppView;
import com.rijatru.flickerclient.viewModel.factories.interfaces.ListAdapterFactory;
import com.rijatru.flickerclient.viewModel.interfaces.PhotosSearchViewMvvm;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class SearchFragmentViewModel implements PhotosSearchViewMvvm.ViewModel {

    private final App app;
    private final PhotosSearchViewMvvm.View view;
    private final ListAdapterFactory listAdapterFactory;
    private final PhotosManager photosManager;

    private ListAdapter adapter;
    private CompositeDisposable disposables = new CompositeDisposable();
    private ObservableField<String> searchTerm;

    public SearchFragmentViewModel(App app, AppView view, ListAdapterFactory listAdapterFactory, PhotosManager photosManager) {

        this.view = (PhotosSearchViewMvvm.View) view;

        this.app = app;
        this.listAdapterFactory = listAdapterFactory;
        this.photosManager = photosManager;

        searchTerm = new ObservableField<>();
    }

    @Override
    public void subscribe() {

        disposables.add(photosManager.getSearchObservable()
                .doOnNext(this::initAdapterAndSendToView)
                .subscribe());
    }

    private void initAdapterAndSendToView(List<Photo> photos) {

        List<Item> photosListItems = MapperUtil.getPhotosListItems(app, photos, true);

        if (adapter == null) {
            adapter = listAdapterFactory.getListAdapter();
        }

        if (photosManager.isNotFirstPage()) {
            adapter.removeLastItems(3);
            adapter.addNewItems(photosListItems);
        } else {
            adapter.setItems(photosListItems);
        }

        view.onListOfPhotosReady(adapter);
    }

    @Override
    public void unSubscribe() {
        disposables.clear();
    }

    @Override
    public void searchPhotos(String searchTerm) {
        photosManager.clearSearchResults();
        photosManager.searchPhotos(searchTerm);
    }

    @Override
    public String getSearchTerm() {
        return searchTerm.get();
    }

    @Override
    public void setSearchTerm(String searchTerm) {
        this.searchTerm.set(searchTerm);
    }

    @Override
    public void searchMorePhotos() {
        photosManager.searchMorePhotos(searchTerm.get());
    }
}
