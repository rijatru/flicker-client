package com.rijatru.flickerclient.viewModel;


import android.view.View;

import com.rijatru.flickerclient.viewModel.interfaces.ToolbarMvvm;

public class ToolbarViewModel implements ToolbarMvvm.ViewModel {

    private final ToolbarMvvm.View toolBar;

    public ToolbarViewModel(ToolbarMvvm.View toolBar) {
        this.toolBar = toolBar;
    }

    @Override
    public void onLeftIconClick(View view) {
        toolBar.onLeftIconClick();
    }

    @Override
    public void onRightEndIconClick(View view) {
        toolBar.onRightEndIconClick();
    }
}
