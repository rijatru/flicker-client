package com.rijatru.flickerclient.viewModel.factories;

import com.rijatru.flickerclient.model.data.LogManager;
import com.rijatru.flickerclient.view.adapters.AppBaseListAdapter;
import com.rijatru.flickerclient.view.adapters.interfaces.ListAdapter;
import com.rijatru.flickerclient.view.factories.interfaces.ViewFactory;
import com.rijatru.flickerclient.viewModel.factories.interfaces.ListAdapterFactory;

public class AppListAdapterFactory implements ListAdapterFactory {

    private ViewFactory viewFactory;
    private LogManager logManager;

    public AppListAdapterFactory(ViewFactory viewFactory, LogManager logManager) {
        this.viewFactory = viewFactory;
        this.logManager = logManager;
    }

    @Override
    public ListAdapter getListAdapter() {
        return new AppBaseListAdapter(viewFactory, logManager);
    }
}
