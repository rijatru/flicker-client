package com.rijatru.flickerclient.viewModel.factories;

import com.rijatru.flickerclient.App;
import com.rijatru.flickerclient.model.data.LogManager;
import com.rijatru.flickerclient.model.data.interfaces.Bus;
import com.rijatru.flickerclient.model.data.interfaces.PersistenceManager;
import com.rijatru.flickerclient.model.data.interfaces.PhotosManager;
import com.rijatru.flickerclient.model.data.interfaces.ServiceManager;
import com.rijatru.flickerclient.model.interfaces.Item;
import com.rijatru.flickerclient.view.interfaces.AppView;
import com.rijatru.flickerclient.viewModel.PhotoDetailFragmentViewModel;
import com.rijatru.flickerclient.viewModel.PhotoListItemViewModel;
import com.rijatru.flickerclient.viewModel.PhotosFragmentViewModel;
import com.rijatru.flickerclient.viewModel.SearchFragmentViewModel;
import com.rijatru.flickerclient.viewModel.factories.interfaces.ListAdapterFactory;
import com.rijatru.flickerclient.viewModel.factories.interfaces.ViewModelFactory;
import com.rijatru.flickerclient.viewModel.interfaces.AppViewModel;

import javax.inject.Inject;

public class AppViewModelFactory implements ViewModelFactory {

    public static final int PHOTOS_LIST_CONTAINER_VIEW_MODEL = 100;
    public static final int PHOTO_LIST_ITEM_VIEW_MODEL = 200;
    public static final int PHOTO_DETAIL_VIEW_MODEL = 300;
    public static final int PHOTO_SEARCH_VIEW_MODEL = 400;

    private final App app;
    private final ServiceManager serviceManager;
    private final PersistenceManager persistenceManager;
    private final ListAdapterFactory listAdapterFactory;
    private final LogManager logManager;
    private final Bus bus;
    private final PhotosManager photosManager;

    @Inject
    public AppViewModelFactory(App app, ServiceManager serviceManager, PersistenceManager persistenceManager, ListAdapterFactory listAdapterFactory, LogManager logManager, Bus bus, PhotosManager photosManager) {
        this.app = app;
        this.serviceManager = serviceManager;
        this.persistenceManager = persistenceManager;
        this.listAdapterFactory = listAdapterFactory;
        this.logManager = logManager;
        this.bus = bus;
        this.photosManager = photosManager;
    }

    @Override
    public AppViewModel getViewModel(AppView view, int type) {

        AppViewModel viewModel;

        switch (type) {

            case PHOTOS_LIST_CONTAINER_VIEW_MODEL:
                viewModel = new PhotosFragmentViewModel(app, view, listAdapterFactory, photosManager);
                break;
            case PHOTO_DETAIL_VIEW_MODEL:
                viewModel = new PhotoDetailFragmentViewModel(app, view, photosManager);
                break;
            case PHOTO_SEARCH_VIEW_MODEL:
                viewModel = new SearchFragmentViewModel(app, view, listAdapterFactory, photosManager);
                break;
            default:
                viewModel = null;
        }

        return viewModel;
    }

    @Override
    public AppViewModel getItemViewModel(AppView view, int type, Item item) {

        AppViewModel viewModel;

        switch (type) {

            case PHOTO_LIST_ITEM_VIEW_MODEL:
                viewModel = new PhotoListItemViewModel(view, item, bus);
                break;
            default:
                viewModel = null;
        }

        return viewModel;
    }
}
