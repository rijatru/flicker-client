package com.rijatru.flickerclient.viewModel.factories.interfaces;

import com.rijatru.flickerclient.view.adapters.interfaces.ListAdapter;

public interface ListAdapterFactory {

    ListAdapter getListAdapter();
}
