package com.rijatru.flickerclient.viewModel.factories.interfaces;

import com.rijatru.flickerclient.model.interfaces.Item;
import com.rijatru.flickerclient.view.interfaces.AppView;
import com.rijatru.flickerclient.viewModel.interfaces.AppViewModel;

public interface ViewModelFactory {

    AppViewModel getViewModel(AppView view, int type);

    AppViewModel getItemViewModel(AppView view, int type, Item item);
}
