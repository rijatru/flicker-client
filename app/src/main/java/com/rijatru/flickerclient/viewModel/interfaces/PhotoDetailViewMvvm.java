package com.rijatru.flickerclient.viewModel.interfaces;

import com.rijatru.flickerclient.view.interfaces.AppView;

public interface PhotoDetailViewMvvm {

    interface View extends AppView {

    }

    interface ViewModel extends AppViewModel {

        String getImage();

        String getTitle();

        String getId();

        void subscribe();

        void unSubscribe();

        void setPhotoId(long photoId);
    }
}
