package com.rijatru.flickerclient.viewModel.interfaces;

import com.rijatru.flickerclient.view.interfaces.AppView;

public interface PhotoItemViewMvvm {

    interface View extends AppView {

    }

    interface ViewModel extends AppViewModel {

        String getImage();

        String getTitleString();

        void onClick(android.view.View view);
    }
}
