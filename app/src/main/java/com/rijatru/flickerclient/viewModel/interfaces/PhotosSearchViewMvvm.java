package com.rijatru.flickerclient.viewModel.interfaces;

import com.rijatru.flickerclient.view.adapters.interfaces.ListAdapter;
import com.rijatru.flickerclient.view.interfaces.AppView;

public interface PhotosSearchViewMvvm {

    interface View extends AppView {

        void onListOfPhotosReady(ListAdapter adapter);
    }

    interface ViewModel extends AppViewModel {

        void subscribe();

        void unSubscribe();

        void searchPhotos(String s);

        String getSearchTerm();

        void setSearchTerm(String searchTerm);

        void searchMorePhotos();
    }
}
