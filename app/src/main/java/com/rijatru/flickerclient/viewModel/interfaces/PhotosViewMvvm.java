package com.rijatru.flickerclient.viewModel.interfaces;

import com.rijatru.flickerclient.view.adapters.interfaces.ListAdapter;
import com.rijatru.flickerclient.view.interfaces.AppView;

public interface PhotosViewMvvm {

    interface View extends AppView {

        void onListOfPhotosReady(ListAdapter adapter);

        void onNewPhotos();

        void onPhotosLoaded();
    }

    interface ViewModel extends AppViewModel {

        void subscribe();

        void unSubscribe();

        void loadMorePhotos();

        void getNewPhotos();
    }
}
