package com.rijatru.flickerclient.viewModel.interfaces;


import com.rijatru.flickerclient.view.interfaces.ToolBarListener;

public interface ToolbarMvvm {

    interface View {

        void setListener(ToolBarListener listener);

        void onLeftIconClick();

        void onRightEndIconClick();
    }

    interface ViewModel {

        void onLeftIconClick(android.view.View view);

        void onRightEndIconClick(android.view.View view);
    }
}
